unit uMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Buttons, TGA, ShellAPI, jpeg;

type
  TFormMain = class(TForm)
    SpeedButton1: TSpeedButton;
    SpeedButton3: TSpeedButton;
    Image1: TImage;
    SpeedButton4: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

{$R *.dfm}

procedure TFormMain.SpeedButton1Click(Sender: TObject);
begin
if FileExists(ExtractFilePath(ParamStr(0))+'WW1_TEB.exe')
  then
    begin
    ShellExecute(0,'',PChar(ExtractFilePath(ParamStr(0))+'WW1_TEB.exe'),'','',1);
    Close;
    end
  else Application.MessageBox(PChar('������ ��� ������ ����� ��� ���� �� ������'
    +#13+ExtractFilePath(ParamStr(0))+'WW1_TEB.exe'),PChar(FormMain.Caption),MB_ICONERROR+mb_OK);
end;

procedure TFormMain.SpeedButton4Click(Sender: TObject);
begin
if FileExists(ExtractFilePath(ParamStr(0))+'Help\WW1_TEB_Help.chm')
  then ShellExecute(0,'',PChar(ExtractFilePath(ParamStr(0))+'Help\WW1_TEB_Help.chm'),'','',1)
  else Application.MessageBox(PChar('������ ��� ������ ����� ��� ���� �� ������'
    +#13+ExtractFilePath(ParamStr(0))+'Help\WW1_TEB_Help.chm'),PChar(FormMain.Caption),MB_ICONERROR+mb_OK);
end;

procedure TFormMain.SpeedButton3Click(Sender: TObject);
begin
Close;
end;

procedure TFormMain.SpeedButton2Click(Sender: TObject);
begin
if FileExists(ExtractFilePath(ParamStr(0))+'Install\install_flash_player.exe')
  then ShellExecute(0,'',PChar(ExtractFilePath(ParamStr(0))+'Install\install_flash_player.exe'),'','',1)
  else Application.MessageBox(PChar('������ ��� ������ ����� ��� ���� �� ������'
    +#13+ExtractFilePath(ParamStr(0))+'Install\install_flash_player.exe'),PChar(FormMain.Caption),
    MB_ICONERROR+mb_OK);
end;

procedure TFormMain.SpeedButton5Click(Sender: TObject);
begin
if FileExists(ExtractFilePath(ParamStr(0))+'Utilities\RegisterOCX.exe')
  then ShellExecute(0,'',PChar(ExtractFilePath(ParamStr(0))+'Utilities\RegisterOCX.exe'),'','',1)
  else Application.MessageBox(PChar('������ ��� ������ ����� ��� ���� �� ������'
    +#13+ExtractFilePath(ParamStr(0))+'Utilities\RegisterOCX.exe'),PChar(FormMain.Caption),
    MB_ICONERROR+mb_OK);
end;

procedure TFormMain.SpeedButton6Click(Sender: TObject);
begin
ShellExecute(0,'',PChar(ExtractFilePath(ParamStr(0))),'','',1)
end;

end.
