unit uMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,ShockwaveFlashObjects_TLB, StdCtrls, Buttons, ShellAPI;

type
  TFormMain = class(TForm)
    SpeedButton1: TSpeedButton;
    Edit1: TEdit;
    SpeedButton2: TSpeedButton;
    OpenDialog1: TOpenDialog;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

{$R *.DFM}


procedure TFormMain.SpeedButton1Click(Sender: TObject);
begin
WinExec(PChar('regsvr32 /s ' + Edit1.Text), SW_HIDE);
end;

procedure TFormMain.SpeedButton2Click(Sender: TObject);
begin
if OpenDialog1.Execute then
  begin
  Edit1.Text:=OpenDialog1.FileName;
  end;
end;

procedure TFormMain.SpeedButton3Click(Sender: TObject);
begin
if FileExists(ExtractFilePath(ParamStr(0))+'Flash8_ocx.exe')
  then ShellExecute(0,'',PChar(ExtractFilePath(ParamStr(0))+'Flash8_ocx.exe'),'','',1)
  else Application.MessageBox(PChar('������ ��� ������ ����� ��� ���� �� ������'
    +#13+ExtractFilePath(ParamStr(0))+'Flash8_ocx.exe'),PChar(FormMain.Caption),
    MB_ICONERROR+mb_OK);
end;

procedure TFormMain.SpeedButton4Click(Sender: TObject);
begin
Application.MessageBox(PChar('��� ����, ����� ���������������� Flash8.ocx ����:'+#10#13#10#13
  +'����������� ��� � ����������'+#10#13
  +'c:\Windows\System32\Macromed\Flash\'+#10#13
  +'��� ����� �������, ����� �� ������ Copy OCX'+#10#13#10#13
  +'����� ���������������� ����������������,'+#10#13
  +'������� ��� ���� (�� ���������'+#10#13
  +'c:\Windows\System32\Macromed\Flash\Flash8.ocx)'+#10#13
  +'� ������ �� ������ Register OCX'+#10#13),
  PChar('Register OCX'),MB_IconInformation+MB_OK)
end;

end.
