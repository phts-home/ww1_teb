object FormMain: TFormMain
  Left = 270
  Top = 259
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'RegisterOCX'
  ClientHeight = 101
  ClientWidth = 496
  Color = clBlack
  Font.Charset = DEFAULT_CHARSET
  Font.Color = 2566091
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton1: TSpeedButton
    Left = 20
    Top = 55
    Width = 116
    Height = 26
    Caption = 'Register OCX'
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 2566091
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    OnClick = SpeedButton1Click
  end
  object SpeedButton2: TSpeedButton
    Left = 445
    Top = 55
    Width = 26
    Height = 26
    Caption = '...'
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 2566091
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    OnClick = SpeedButton2Click
  end
  object SpeedButton3: TSpeedButton
    Left = 20
    Top = 15
    Width = 116
    Height = 26
    Caption = 'Copy OCX'
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 2566091
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    OnClick = SpeedButton3Click
  end
  object SpeedButton4: TSpeedButton
    Left = 445
    Top = 15
    Width = 26
    Height = 26
    Caption = '?'
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 2566091
    Font.Height = -16
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    OnClick = SpeedButton4Click
  end
  object Edit1: TEdit
    Left = 150
    Top = 60
    Width = 291
    Height = 16
    BorderStyle = bsNone
    TabOrder = 0
    Text = 'c:\Windows\System32\Macromed\Flash\Flash8.ocx'
  end
  object OpenDialog1: TOpenDialog
    Filter = 'OCX File|*.ocx'
    Left = 145
    Top = 15
  end
end
