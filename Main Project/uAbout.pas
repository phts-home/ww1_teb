unit uAbout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TGA, ExtCtrls;

type
  TFormAbout = class(TForm)
    Image1: TImage;
    PanelClose2: TPanel;
    ImgNormClose: TImage;
    ImgHighClose: TImage;
    PanelClose: TPanel;
    ImgNormCls: TImage;
    ImgHighLCls: TImage;
    procedure ImgHighCloseClick(Sender: TObject);
    procedure ImgHighLClsClick(Sender: TObject);
    procedure ImgNormClsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormCloseMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
  public
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
  end;

var
  FormAbout: TFormAbout;

implementation

{$R *.dfm}

procedure TFormAbout.WMNCHitTest(var M: TWMNCHitTest);
begin
inherited;
if M.Result = htClient then M.Result := htCaption;
ImgHighClose.Visible:=False;
ImgHighLCls.Visible:=False;
end;

procedure TFormAbout.ImgHighCloseClick(Sender: TObject);
begin
Close;
end;

procedure TFormAbout.ImgHighLClsClick(Sender: TObject);
begin
Close;
end;

procedure TFormAbout.ImgNormClsMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighLCls.Visible:=True;
end;

procedure TFormAbout.ImgNormCloseMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighClose.Visible:=True;
end;

procedure TFormAbout.FormShow(Sender: TObject);
begin
ImgHighLCls.Visible:=False;
ImgHighClose.Visible:=False;
end;

procedure TFormAbout.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = 27 then Close;
end;

end.
