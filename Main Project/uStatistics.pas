unit uStatistics;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, TGA, ComCtrls;

type
  TFormStat = class(TForm)
    ListView1: TListView;
    Image1: TImage;
    PanelClose: TPanel;
    ImgNormCls: TImage;
    ImgHighLCls: TImage;
    PanelClose2: TPanel;
    ImgNormClose: TImage;
    ImgHighClose: TImage;
    procedure ImgHighCloseClick(Sender: TObject);
    procedure ImgNormCloseMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgHighLClsClick(Sender: TObject);
    procedure ImgNormClsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure ListView1DblClick(Sender: TObject);
    procedure ListView1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
  public
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
  end;

var
  FormStat: TFormStat;

implementation

uses uMemo;

{$R *.dfm}

procedure TFormStat.WMNCHitTest(var M: TWMNCHitTest);
begin
inherited;
if M.Result = htClient then M.Result := htCaption;
ImgHighLCls.Visible:=False;
ImgHighClose.Visible:=False;
end;

//---- ������ ----

//������� [x]
procedure TFormStat.ImgHighLClsClick(Sender: TObject);
begin
Close;
end;

//�������
procedure TFormStat.ImgHighCloseClick(Sender: TObject);
begin
Close;
end;

//---- ����� ������ ----


//---- ��������� ----

procedure TFormStat.ImgNormClsMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
ImgHighLCls.Visible:=True;
end;

procedure TFormStat.ImgNormCloseMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
ImgHighClose.Visible:=True;
end;

//---- ����� ��������� ----


procedure TFormStat.FormShow(Sender: TObject);
begin
ListView1.Columns[0].Width:=120;
ListView1.Columns[1].Width:=130;
ListView1.Columns[2].Width:=100;
ListView1.Columns[3].Width:=100;
ListView1.Columns[4].Width:=150;
ListView1.Columns[5].Width:=130;
ListView1.Columns[6].Width:=200;
ListView1.ItemIndex:=0;
ImgHighLCls.Visible:=False;
ImgHighClose.Visible:=False;
end;

procedure TFormStat.ListView1DblClick(Sender: TObject);
begin
if ListView1.Items[ListView1.ItemIndex].Caption<>'' then
  begin
  FormMemo.Memo1.Clear;
  FormMemo.Memo1.Lines.Add(ListView1.Items[ListView1.ItemIndex].Caption);
  FormMemo.Memo1.Lines.Add('');
  FormMemo.Memo1.Lines.Add('����� ��������������');
  FormMemo.Memo1.Lines.Add(ListView1.Items[ListView1.ItemIndex].SubItems[0]);
  FormMemo.Memo1.Lines.Add('');
  FormMemo.Memo1.Lines.Add('��������');
  FormMemo.Memo1.Lines.Add(ListView1.Items[ListView1.ItemIndex].SubItems[1]);
  FormMemo.Memo1.Lines.Add('');
  FormMemo.Memo1.Lines.Add('��������');
  FormMemo.Memo1.Lines.Add(ListView1.Items[ListView1.ItemIndex].SubItems[2]);
  FormMemo.Memo1.Lines.Add('');
  FormMemo.Memo1.Lines.Add('��������� ��� ����� � �������');
  FormMemo.Memo1.Lines.Add(ListView1.Items[ListView1.ItemIndex].SubItems[3]);
  FormMemo.Memo1.Lines.Add('');
  FormMemo.Memo1.Lines.Add('����� ������������');
  FormMemo.Memo1.Lines.Add(ListView1.Items[ListView1.ItemIndex].SubItems[4]);
  FormMemo.Memo1.Lines.Add('');
  FormMemo.Memo1.Lines.Add('% ������������ �� ��������������');
  FormMemo.Memo1.Lines.Add(ListView1.Items[ListView1.ItemIndex].SubItems[5]);

  FormMemo.ShowModal;
  end;
end;

procedure TFormStat.ListView1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if Button = mbRight then
  begin
  ListView1DblClick(ListView1);
  end;
end;

procedure TFormStat.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = 27 then Close;
end;

end.
