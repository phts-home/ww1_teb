unit uHrono;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, TGA, ComCtrls, StdCtrls, Grids, OleCtrls, SHDocVw;

type
  TFormHrono = class(TForm)
    Image1: TImage;
    PanelClose2: TPanel;
    ImgNormClose: TImage;
    ImgHighClose: TImage;
    PanelClose: TPanel;
    ImgNormCls: TImage;
    ImgHighLCls: TImage;
    WebBrowser1: TWebBrowser;
    procedure ImgHighCloseClick(Sender: TObject);
    procedure ImgHighLClsClick(Sender: TObject);
    procedure ImgNormClsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormCloseMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
  public
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
  end;

var
  FormHrono: TFormHrono;

implementation

uses uMemo;

{$R *.dfm}

procedure TFormHrono.WMNCHitTest(var M: TWMNCHitTest);
begin
inherited;
if M.Result = htClient then M.Result := htCaption;
ImgHighLCls.Visible:=False;
ImgHighClose.Visible:=False;
end;

//---- ������ ----

//������� [x]
procedure TFormHrono.ImgHighLClsClick(Sender: TObject);
begin
Close;
end;

//�������
procedure TFormHrono.ImgHighCloseClick(Sender: TObject);
begin
Close;
end;

//---- ����� ������ ----


//---- ��������� ----

procedure TFormHrono.ImgNormClsMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
ImgHighLCls.Visible:=True;
end;

procedure TFormHrono.ImgNormCloseMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
ImgHighClose.Visible:=True;
end;

//---- ����� ��������� ----


procedure TFormHrono.FormShow(Sender: TObject);
begin
WebBrowser1.Navigate(ExtractFilePath(ParamStr(0))+'Data\Chronology\File000.html');
ImgHighLCls.Visible:=False;
ImgHighClose.Visible:=False;
end;

procedure TFormHrono.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = 27 then Close;
end;

end.
