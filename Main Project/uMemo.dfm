object FormMemo: TFormMemo
  Left = 175
  Top = 108
  AutoSize = True
  BorderStyle = bsNone
  Caption = 'FormMemo'
  ClientHeight = 254
  ClientWidth = 348
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 0
    Top = 0
    Width = 348
    Height = 254
    AutoSize = True
  end
  object Memo1: TMemo
    Left = 15
    Top = 25
    Width = 316
    Height = 156
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
    OnKeyDown = Memo1KeyDown
  end
  object PanelClose: TPanel
    Left = 332
    Top = 2
    Width = 10
    Height = 11
    BevelOuter = bvNone
    Caption = 'PanelClose'
    TabOrder = 1
    object ImgNormCls: TImage
      Left = 0
      Top = 0
      Width = 10
      Height = 11
      AutoSize = True
      Picture.Data = {
        0954544741496D61676596010000424D96010000000000003600000028000000
        0A0000000B000000010018000000000060010000000000000000000000000000
        000000001E1E591E1E591E1E591E1E591E1E591E1E591E1E591E1E591E1E591E
        1E59000023236533338A26266B26266B1E1E591E1E591E1E5926266B29297123
        236500001E1E592B2B774646B43B3B9C2323651E1E5933338A4646B42E2E7D1E
        1E5900001E1E591E1E592E2E7D4646B43E3EA22E2E7D4646B43131831E1E591E
        1E5900001E1E591E1E5921215F3B3B9C4646B44646B44646B42E2E7D1E1E591E
        1E5900001E1E591E1E591E1E592323654343AE4646B42E2E7D1E1E591E1E591E
        1E5900001E1E591E1E591E1E593939964646B44646B43B3B9C26266B1E1E591E
        1E5900001E1E591E1E5926266B3636904646B44141A84646B42E2E7D1E1E591E
        1E5900001E1E591E1E593939964646B436369026266B3E3EA24646B42B2B771E
        1E5900001E1E5929297131318333338A21215F1E1E5929297136369033338A1E
        1E5900001E1E591E1E591E1E591E1E591E1E591E1E591E1E591E1E591E1E591E
        1E590000}
      OnMouseMove = ImgNormClsMouseMove
    end
    object ImgHighLCls: TImage
      Left = 0
      Top = 0
      Width = 10
      Height = 11
      AutoSize = True
      Picture.Data = {
        0954544741496D61676596010000424D96010000000000003600000028000000
        0A0000000B000000010018000000000060010000000000000000000000000000
        000000001E1E591E1E591E1E591E1E591E1E591E1E591E1E591E1E591E1E591E
        1E5900001E1E6B1F1F9F1E1E731E1E731E1E591E1E591E1E591E1E731F1F7C1E
        1E6B00001E1E591F1F852020DD1F1FBA1E1E6B1E1E591F1F9F2020DD1F1F8E1E
        1E5900001E1E591E1E591F1F8E2020DD2020C31F1F8E2020DD1F1F971E1E591E
        1E5900001E1E591E1E591E1E621F1FBA2020DD2020DD2020DD1F1F8E1E1E591E
        1E5900001E1E591E1E591E1E591E1E6B2020D42020DD1F1F8E1E1E591E1E591E
        1E5900001E1E591E1E591E1E591F1FB12020DD2020DD1F1FBA1E1E731E1E591E
        1E5900001E1E591E1E591E1E731F1FA82020DD2020CB2020DD1F1F8E1E1E591E
        1E5900001E1E591E1E591F1FB12020DD1F1FA81E1E732020C32020DD1F1F851E
        1E5900001E1E591F1F7C1F1F971F1F9F1E1E621E1E591F1F7C1F1FA81F1F9F1E
        1E5900001E1E591E1E591E1E591E1E591E1E591E1E591E1E591E1E591E1E591E
        1E590000}
      Visible = False
      OnClick = ImgHighLClsClick
    end
  end
  object PanelClose2: TPanel
    Left = 240
    Top = 200
    Width = 78
    Height = 24
    AutoSize = True
    BevelOuter = bvNone
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 2
    object ImgNormClose: TImage
      Left = 0
      Top = 0
      Width = 78
      Height = 24
      AutoSize = True
      Picture.Data = {
        0954544741496D61676556160000424D56160000000000003600000028000000
        4E00000018000000010018000000000020160000000000000000000000000000
        00000000E0E5F0E0E5EFE0E5EFE0E4EFDFE4EFDFE4EFDFE3EEDFE3EEDEE3EEDE
        E3EEDEE3EEDDE2EDDDE2EDDDE2EDDDE2EDDDE1ECDCE1ECDCE1ECDCE0ECDCE0EC
        DBE0ECDBE0EBDBE0EBDADFEBDADFEBDADFEBDADFEADADEEAD9DEEAD9DEEAD9DE
        E9D8DDE9D8DDE9D8DDE9D8DDE9D7DCE9D7DCE8D7DCE8D7DCE8D6DBE8D6DBE8D6
        DBE7D6DBE7D5DAE7D5DAE7D5DAE6D5DAE6D4DAE6D4D9E6D4D9E6D4D9E6D4D9E6
        D3D8E5D3D8E5D3D8E5D3D8E5D2D8E5D2D7E4D2D7E4D2D7E4D1D7E4D1D7E4D1D6
        E3D1D6E3D0D6E3D0D6E3D0D6E3D0D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2CF
        D4E2CFD4E2CED4E1CED4E1CED4E10000E0E5EFE0E5EFE0E4EFE0E4EFDFE4EFDF
        E4EFDFE3EEDEE3EEDEE3EEDEE3EEDEE3EDDDE2EDDDE2EDDDE2EDDDE1EDDCE1EC
        DCE1ECDCE1ECDCE0ECDBE0ECDBE0ECDBE0EBDBE0EBDADFEBDADFEBDADFEADADE
        EAD9DEEAD9DEEAD9DEEAD9DDE9D8DDE9D8DDE9D8DDE9D7DDE9D7DCE8D7DCE8D7
        DCE8D7DCE8D6DBE8D6DBE7D6DBE7D6DBE7D5DAE7D5DAE7D5DAE6D4DAE6D4DAE6
        D4D9E6D4D9E6D4D9E6D3D9E5D3D8E5D3D8E5D3D8E5D2D8E5D2D7E4D2D7E4D2D7
        E4D2D7E4D1D7E4D1D7E4D1D6E3D1D6E3D0D6E3D0D6E3D0D5E3D0D5E3D0D5E3D0
        D5E2CFD5E2CFD4E2CFD4E2CFD4E2CFD4E2CED4E1CED4E1CED4E10000E0E5EFE0
        E5EFE0E4EFDFE4EFDFE4EFDFE3EEDFE3EEDEE3EEDEE3EEDEE3EEDDE2EDDDE2ED
        DDE2EDDDE2EDDDE1ECDCE1ECDCE1ECDCE0ECDBE0ECDBE0ECDBE0EBDBE0EBDADF
        EBDADFEBDADFEBDADFEADADEEAD9DEEAD9DEEAD9DEE9D8DDE9D8DDE9D8DDE9D8
        DDE9D7DCE9D7DCE8D7DCE8D7DCE8D6DBE8D6DBE8D6DBE7D6DBE7D5DAE7D5DAE7
        D5DAE6D5DAE6D4DAE6D4D9E6D4D9E6D4D9E6D4D9E6D3D8E5D3D8E5D3D8E5D3D8
        E5D2D7E5D2D7E4D2D7E4D2D7E4D1D7E4D1D7E4D1D6E3D1D6E3D0D6E3D0D6E3D0
        D6E3D0D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2CFD4E2CFD4E2CED4E1CED4E1
        CED4E1CED3E10000E0E5EFE0E4EFE0E4EFDFE4EFDFE4EFDFE3EEDEE3EEDEE3EE
        DEE3EEDEE3EDDDE2EDDDE2EDDDE2EDDDE1EDDCE1ECDCE1ECDCE1ECDCE0ECDBE0
        ECDBE0ECDBE0EBDBE0EBDADFEBDADFEBDADFEADADEEAD9DEEAD9DEEAD9DEEAD9
        DDE9D8DDE9D8DDE9D8DDE9D7DDE973757C393B3E1D1D1F73757CD6DBE8D6DBE7
        D6DBE7D6DBE7D5DAE7D5DAE7D5DAE6D4DAE6D4DAE6D4D9E6D4D9E6D4D9E6D3D9
        E5D3D8E5D3D8E5D3D8E5D2D8E5D2D7E4D2D7E4D2D7E4D2D7E4D1D7E4D1D7E4D1
        D6E3D1D6E3D0D6E3D0D6E3D0D5E3D0D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2
        CFD4E2CED4E2CED4E1CED4E1CED3E1CED3E10000E0E5EFE0E4EFDFE4EFDFE4EF
        DFE3EEDFE3EEDEE3EEDEE3EEDEE3EEDDE2EDDDE2EDDDE2EDDDE2EDDDE1ECDCE1
        ECDCE1ECDCE0ECDBE0ECDBE0ECDBE0EBDBE0EBDADFEBDADFEBDADFEBDADFEAD9
        DEEAD9DEEAD9DEEAD9DDE9D8DDE9D8DDE9D8DDE9D8DDE9D7DCE99EA1AA000000
        00000072757CD6DBE8D6DBE7D6DBE7D5DAE7D5DAE7D5DAE6D5DAE6D4DAE6D4D9
        E6D4D9E6D4D9E6D4D9E6D3D8E5D3D8E5D3D8E5D3D8E5D2D7E5D2D7E4D2D7E4D2
        D7E4D1D7E4D1D7E4D1D6E3D1D6E3D0D6E3D0D6E3D0D6E3D0D5E3D0D5E3D0D5E2
        CFD5E2CFD4E2CFD4E2CFD4E2CFD4E2CED4E1CED4E1CED4E1CED3E1CED3E10000
        E0E4EFE0E4EFDFE4EFDFE4EFDFE3EEDEE3EEDEE3EEDEE3EEDEE3EDDDE2EDDDE2
        EDDDE2EDDDE1EDDCE1ECDCE1ECDCE1ECDCE0ECDBE0ECDBE0ECDBE0EBDADFEBDA
        DFEBDADFEBDADFEADADEEAD9DEEAD9DEEAD9DEEAD9DDE9D8DDE9D8DDE9D8DDE9
        D7DDE9D7DCE8D7DCE80E0F0F00000080838BD6DBE7D6DBE7D6DBE7D5DAE7D5DA
        E7D5DAE6D4DAE6D4DAE6D4D9E6D4D9E6D4D9E6D3D9E5D3D8E5D3D8E5D3D8E5D2
        D8E5D2D7E4D2D7E4D2D7E4D2D7E4D1D7E4D1D7E4D1D6E3D1D6E3D0D6E3D0D6E3
        D0D5E3D0D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2CFD4E2CED4E2CED4E1CED4
        E1CED3E1CED3E1CED3E10000E0E4EFDFE4EFDFE4EFDFE3EEDFE3EEDEE3EEDEE3
        EEDEE3EEDDE2EDDDE2EDDDE2EDDDE2EDDDE1ECDCE1ECDCE1ECDCE0ECDBE0ECDB
        E0ECDBE0EBDBE0EBDADFEBDADFEBDADFEBDADFEAD9DEEAD9DEEAD9DEEAD9DDE9
        D8DDE9D8DDE9D8DDE9D8DDE9D7DCE9D7DCE8BABFC9000000000000B9BEC9D6DB
        E7D6DBE7D5DAE7D5DAE7D5DAE6D5DAE6D4DAE6D4D9E6D4D9E6D4D9E6D4D9E6D3
        D8E5D3D8E5D3D8E5D3D8E5D2D7E5D2D7E4D2D7E4D2D7E4D1D7E4D1D7E4D1D6E3
        D1D6E3D0D6E3D0D6E3D0D6E3D0D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2CFD4
        E2CFD4E2CED4E1CED4E1CED4E1CED3E1CED3E1CDD3E10000E0E4EFDFE4EFDFE4
        EF86888F595B5F4A4C4F0000000000002C2D2FB1B5BEDDE2EDDDE1EDDCE1ECDC
        E1ECDCE1ECDCE0EC92959DDBE0ECDBE0EBDADFEBDADFEBA0A4ACA0A4ACDADEEA
        D9DEEAD9DEEAAEB2BBD9DDE9D8DDE9D8DDE9D8DDE9ACB1BABABFC9C9CDD97375
        7C00000000000064666CD6DBE7D6DBE747494D63666CD5DAE6D4DAE6D4DAE6D4
        D9E6D4D9E6D4D9E6D3D9E5D3D8E5D3D8E5D3D8E5D2D8E5D2D7E4D2D7E4D2D7E4
        D2D7E4D1D7E4D1D7E4D1D6E3D1D6E3D0D6E3A6ABB6D0D5E3D0D5E3D0D5E3D0D5
        E2CFD5E2CFD4E2CFD4E2CFD4E2CED4E2CED4E1CED4E1CED3E1CED3E1CED3E1CD
        D3E10000DFE4EFDFE4EF686A6F0000000000000F0F1000000000000000000000
        0000585A5FDDE1ECDCE1ECDCE1ECDCE0EC3A3C3F00000000000092959DBDC1CC
        74777D0000003A3B3EBCC0CB00000000000074767CD8DDE9D8DDE9ADB1BA3A3B
        3E0000000000000000008F939B2B2C2E0000009DA1A90E0F0F00000000000000
        0000393A3DD4DAE6D4D9E69B9FA91C1D1F63656B2A2B2E62656B62656B2A2B2E
        7E8189D2D7E4D2D7E40E0E0F0000000E0E0FC3C8D4D1D6E3D0D6E36F72790000
        001C1C1ED0D5E3D0D5E2CFD5E237393C37393C61636961636937393C37393CCE
        D4E1CED3E1CED3E1CDD3E1CDD3E10000DFE4EFDFE4EFD0D4DE1E1E2000000076
        797F76797E93979E4A4B4F00000000000075787EDCE1EC0F0F10000000000000
        0000000000003A3B3F3A3B3F0000000000002C2C2FD9DEEA3A3B3E0000003A3B
        3ECACED9D8DDE93A3B3E00000000000000000073757CD7DCE847494D0000000E
        0F0F000000000000000000000000000000000000AAAEB8D4D9E6393A3D000000
        00000000000000000000000000000054565BD2D7E446484C000000000000C3C8
        D4D1D6E3D0D6E37D8088000000000000A6AAB6D0D5E2CFD5E237393C00000000
        000037393C0000000000001B1C1E7C7F87CED3E1CDD3E1CDD3E00000DFE4EFDF
        E3EEDFE3EE94979FA3A6AFDEE3EEDDE2EDDDE2EDCED3DD0000000000000F0F10
        75787E000000000000000000AFB3BCDBE0EBDADFEB74777D0000000000007476
        7DAEB2BB0000000000000E0F10D8DDE9484A4E00000000000000000081848BD7
        DCE856585D00000000000000000063666CD5DAE772747B55575C000000000000
        63656BD4D9E663656B00000071737AD3D8E5A9ADB70000001C1D1E70737AD2D7
        E462646A0000002A2B2DD1D6E3D0D6E3D0D6E32A2B2D0000006F7279D0D5E2CF
        D5E2CFD4E237393C000000000000979BA5C0C6D2292A2D000000000000B2B7C3
        CDD3E1CDD3E00000DFE4EFDFE3EEDEE3EEDEE3EEDEE3EEDEE2EDDDE2EDDDE2ED
        93979E0000000000003B3C3F75787E0000000000002C2D2FDBE0EBDADFEBDADF
        EBCBD0DB0F0F1000000074767DD9DEEA3A3B3E0000000E0F102B2C2F00000000
        000000000081848BD7DCE8D7DCE880838B00000000000072757BD5DAE7D5DAE7
        D5DAE6D4DAE6393A3D0000000E0E0FAAAEB8383A3D000000C5CAD6D3D8E55456
        5C0E0E0F0000009A9EA7C4C9D51C1D1E0000000E0E0FB5B9C5D0D6E3D0D6E32A
        2B2D0000008B8E97D0D5E2CFD5E2CFD4E245474B000000000000979BA5898D96
        0E0E0F0000000E0E0FCDD3E1CDD3E0CDD3E00000DFE3EEDFE3EEDEE3EEDEE3EE
        DEE3EE76797E67696F585A5F2C2D2F0000000000000F0F1075777E0000000F0F
        10BEC2CCDBE0EBDADFEBDADFEBDADFEB2C2D2F00000065686DCBCFDA2B2C2F00
        000000000000000000000000000073757CD7DCE8D7DCE8D6DBE872757C000000
        00000080838BD5DAE7D5DAE6D5DAE6D4DAE67F828A00000071747B8D91990000
        00000000000000000000000000000000000000B6BAC6C3C9D51C1D1E00000038
        393DD0D6E3D0D6E3D0D6E32A2B2D00000037393CCFD5E2CFD4E2CFD4E27C7F88
        00000000000000000000000000000000000052545ACDD3E1CDD3E0CDD3E00000
        DFE3EEDEE3EEDEE3EEDEE3EEC0C4CD1D1E202C2D2F0000000000000000006769
        6EB0B4BD67696E00000000000000000066686EDADFEBDADFEBCBD0DA0F0F1000
        0000484A4ED9DEE93A3B3E000000484A4E65676D1D1D1F00000048494DD7DCE8
        D7DCE8D6DBE856585C00000000000072747BD5DAE7D5DAE6D4DAE671747B0000
        0000000047484DB7BCC600000000000054565CD2D8E546484C38393DA8ACB6D2
        D7E4D1D7E454565B0000001C1D1ED0D6E3D0D6E3C2C7D41C1C1E000000000000
        989CA6CFD4E2CFD4E26E71790000000000000000000E0E0F000000292A2DA4A9
        B4CDD3E0CDD3E0CDD2E00000DFE3EEDEE3EEDEE3EEDEE3EECED3DD4A4B4F3B3C
        3F3B3C3F1D1E1F3B3C3F93969DDCE0ECA1A4AD000000000000000000000000CB
        D0DBDADFEA57595E00000000000065686DCBCED90E0F10000000484A4ED8DDE9
        1D1D1F00000000000064676CD6DBE8D6DBE864666C0000000000000000007274
        7BD5DAE671747B2A2B2E0000002A2B2E71747B9B9EA8000000000000383A3DD2
        D7E5D2D7E4D2D7E4D2D7E4D1D7E4D1D7E46F72790000002A2B2DD0D6E37D8088
        61636A0E0E0F0000000000000000000000006E7179616369000000000000A5AA
        B4CED3E1CED3E1CDD3E1CDD3E1CDD3E0CDD3E0CDD2E00000DEE3EEDEE3EEDEE3
        EEDEE2EDDDE2EDDDE2EDDDE2EDDDE1ED3B3C3F494B4FDCE1ECDCE0ECDBE0EC66
        696E00000000000000000066686E91959C0000000000000E0F10CBCFD9D9DDE9
        82858C000000484A4ED7DDE9D7DCE856585D0000001D1D1FB9BEC9D6DBE7B9BE
        C80E0F0F0000001C1D1F00000000000000000000000000000071747BD3D9E5C5
        CAD600000000000062656BD2D7E4D2D7E4D2D7E4D2D7E4D1D7E4D1D6E446474C
        00000045474CC2C8D42A2B2D0000000000000000000000000000006163696163
        69606369000000000000898D96CED3E1CED3E1CDD3E1CDD3E0CDD3E0CDD2E0CD
        D2E00000DEE3EEDEE3EEDEE3EEDDE2EDCED3DDDDE2EDDDE2EDDDE1EC0F0F100F
        0F1093959DDBE0ECDBE0ECDBE0EBBEC2CC3A3B3F000000494A4E0000001D1E1F
        0000009FA3ACD9DDE9CACED93A3B3E73767CD8DDE9D7DCE9D7DCE8C9CDD92B2C
        2E00000072757CD6DBE7C8CCD8393A3E63666C8E919955575C1C1D1F0E0E0F39
        3A3D63656BC6CBD7D3D8E5B7BBC600000000000046484CD2D7E4D2D7E4D2D7E4
        D1D7E4D1D7E4B5B9C51C1D1E0000000E0E0FD0D6E3D0D5E3999CA68B8E97B3B9
        C4B3B8C46E7179CFD4E2CFD4E2292A2D000000000000B3B7C3CED3E1CDD3E1CD
        D3E1CDD3E0CDD3E0CDD2E0CDD2E00000DEE3EEDEE3EEDEE2ED585A5F0000003B
        3C3F93969E67696E00000000000000000083868EDBE0EBDBE0EBDADFEBDADFEB
        DADFEBDADFEADADEEAD9DEEAD9DEEAD9DEE9D9DDE9D8DDE9D8DDE9D8DDE9D7DD
        E9D7DCE8D7DCE8D7DCE8D7DCE8D6DBE8D6DBE7D6DBE7D6DBE7D5DAE7D5DAE7D5
        DAE6D4DAE6D4DAE6D4D9E6D4D9E6D4D9E6D3D9E5D3D8E5D3D8E5A9ADB7D2D8E5
        D2D7E4D2D7E4D2D7E4D2D7E4D1D7E4D1D6E4D1D6E3D1D6E3D0D6E3D0D6E3D0D5
        E3D0D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2CFD4E2CED4E2979BA56E7178CE
        D3E1CED3E1CED3E1CDD3E1CDD3E0CDD3E0CDD2E0CDD2E0CDD2E00000DEE3EEDE
        E3EEDDE2ED585A5F0000000000000000000000000000000000000F0F10CCD1DC
        DBE0EBDBE0EBDADFEBDADFEBDADFEADADFEAD9DEEAD9DEEAD9DEEAD9DDE9D8DD
        E9D8DDE9D8DDE9D8DDE9D7DCE9D7DCE8D7DCE8D7DCE8D6DBE8D6DBE8D6DBE7D6
        DBE7D5DAE7D5DAE7D5DAE6D5DAE6D4DAE6D4D9E6D4D9E6D4D9E6D4D9E6D3D8E5
        D3D8E5D3D8E5D3D8E5D2D7E5D2D7E4D2D7E4D2D7E4D1D7E4D1D7E4D1D6E3D1D6
        E3D0D6E3D0D6E3D0D6E3D0D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2CFD4E2CF
        D4E2CED4E1CED4E1CED4E1CED3E1CED3E1CDD3E1CDD3E1CDD3E0CDD3E0CDD2E0
        CDD2E0CDD2E00000DEE3EEDEE2EDCED3DD3B3C3F000000000000000000000000
        00000000000092959DDBE0EBDBE0EBDADFEBDADFEBDADFEBDADFEADADEEAD9DE
        EAD9DEEAD9DEE9D9DDE9D8DDE9D8DDE9D8DDE9D7DDE9D7DCE8D7DCE8D7DCE8D7
        DCE8D6DBE8D6DBE7D6DBE7D5DAE7D5DAE7D5DAE7D5DAE6D4DAE6D4DAE6D4D9E6
        D4D9E6D4D9E6D3D9E5D3D8E5D3D8E5D3D8E5D2D8E5D2D7E4D2D7E4D2D7E4D1D7
        E4D1D7E4D1D6E4D1D6E3D1D6E3D0D6E3D0D6E3D0D5E3D0D5E3D0D5E3D0D5E2CF
        D5E2CFD4E2CFD4E2CFD4E2CED4E2CED4E1CED4E1CED3E1CED3E1CDD3E1CDD3E1
        CDD3E0CDD3E0CDD2E0CDD2E0CDD2E0CDD2E00000DEE3EEDDE2EDDDE2EDDDE2ED
        DDE2EDDDE1EC75787E1D1E1F75777E83868EDBE0ECDBE0EBDBE0EBDADFEBDADF
        EBDADFEADADFEAD9DEEAD9DEEAD9DEEAD9DDE9D8DDE9D8DDE9D8DDE9D8DDE9D7
        DCE9D7DCE8D7DCE8D7DCE8D6DBE8D6DBE8D6DBE7D6DBE7D5DAE7D5DAE7D5DAE6
        D5DAE6D4DAE6D4D9E6D4D9E6D4D9E6D4D9E6D3D8E5D3D8E5D3D8E5D3D8E5D2D7
        E5D2D7E4D2D7E4D2D7E4D1D7E4D1D7E4D1D6E3D1D6E3D0D6E3D0D6E3D0D6E3D0
        D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2CFD4E2CFD4E2CED4E1CED4E1CED4E1
        CED3E1CED3E1CDD3E1CDD3E1CDD3E0CDD3E0CDD2E0CDD2E0CDD2E0CCD2E00000
        DEE2EDDDE2EDDDE2EDDDE2EDDDE1EDDCE1ECCDD2DC67696EDCE0ECDBE0ECDBE0
        EBDBE0EBDADFEBDADFEBDADFEBDADFEADADEEAD9DEEAD9DEEAD9DEE9D8DDE9D8
        DDE9D8DDE9D8DDE9D7DDE9D7DCE8D7DCE8D7DCE8D7DCE8D6DBE8D6DBE7D6DBE7
        D5DAE7D5DAE7D5DAE7D5DAE6D4DAE6D4D9E6D4D9E6D4D9E6D4D9E6D3D9E5D3D8
        E5D3D8E5D3D8E5D2D8E5D2D7E4D2D7E4D2D7E4D1D7E4D1D7E4D1D6E4D1D6E3D1
        D6E3D0D6E3D0D6E3D0D5E3D0D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2CFD4E2
        CED4E2CED4E1CED4E1CED3E1CED3E1CDD3E1CDD3E1CDD3E0CDD3E0CDD2E0CDD2
        E0CDD2E0CDD2E0CCD2E00000DDE2EDDDE2EDDDE2EDDDE2EDDDE1ECDCE1ECDCE1
        ECDCE0ECDBE0ECDBE0ECDBE0EBDBE0EBDADFEBDADFEBDADFEADADFEAD9DEEAD9
        DEEAD9DEEAD9DDE9D8DDE9D8DDE9D8DDE9D8DDE9D7DCE9D7DCE8D7DCE8D7DCE8
        D6DBE8D6DBE7D6DBE7D6DBE7D5DAE7D5DAE7D5DAE6D5DAE6D4DAE6D4D9E6D4D9
        E6D4D9E6D4D9E6D3D8E5D3D8E5D3D8E5D3D8E5D2D7E5D2D7E4D2D7E4D2D7E4D1
        D7E4D1D7E4D1D6E3D1D6E3D0D6E3D0D6E3D0D5E3D0D5E3D0D5E3D0D5E2CFD5E2
        CFD4E2CFD4E2CFD4E2CFD4E2CED4E1CED4E1CED4E1CED3E1CED3E1CDD3E1CDD3
        E1CDD3E0CDD2E0CDD2E0CDD2E0CDD2E0CCD2E0CCD2E00000DDE2EDDDE2EDDDE2
        EDDDE1EDDCE1ECDCE1ECDCE1ECDCE0ECDBE0ECDBE0EBDBE0EBDADFEBDADFEBDA
        DFEBDADFEADADEEAD9DEEAD9DEEAD9DEE9D8DDE9D8DDE9D8DDE9D8DDE9D7DDE9
        D7DCE8D7DCE8D7DCE8D7DBE8D6DBE8D6DBE7D6DBE7D5DAE7D5DAE7D5DAE7D5DA
        E6D4DAE6D4D9E6D4D9E6D4D9E6D4D9E6D3D9E5D3D8E5D3D8E5D3D8E5D2D8E5D2
        D7E4D2D7E4D2D7E4D1D7E4D1D7E4D1D6E3D1D6E3D1D6E3D0D6E3D0D6E3D0D5E3
        D0D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2CFD4E2CED4E2CED4E1CED4E1CED3
        E1CED3E1CDD3E1CDD3E1CDD3E0CDD3E0CDD2E0CDD2E0CDD2E0CDD2E0CCD2E0CC
        D2E00000}
      OnMouseMove = ImgNormCloseMouseMove
    end
    object ImgHighClose: TImage
      Left = 0
      Top = 0
      Width = 78
      Height = 24
      AutoSize = True
      Picture.Data = {
        0954544741496D61676556160000424D56160000000000003600000028000000
        4E00000018000000010018000000000020160000000000000000000000000000
        00000000E0E5F0E0E5EFE0E5EFE0E4EFDFE4EFDFE4EFDFE3EEDFE3EEDEE3EEDE
        E3EEDEE3EEDDE2EDDDE2EDDDE2EDDDE2EDDDE1ECDCE1ECDCE1ECDCE0ECDCE0EC
        DBE0ECDBE0EBDBE0EBDADFEBDADFEBDADFEBDADFEADADEEAD9DEEAD9DEEAD9DE
        E9D8DDE9D8DDE9D8DDE9D8DDE9D7DCE9D7DCE8D7DCE8D7DCE8D6DBE8D6DBE8D6
        DBE7D6DBE7D5DAE7D5DAE7D5DAE6D5DAE6D4DAE6D4D9E6D4D9E6D4D9E6D4D9E6
        D3D8E5D3D8E5D3D8E5D3D8E5D2D8E5D2D7E4D2D7E4D2D7E4D1D7E4D1D7E4D1D6
        E3D1D6E3D0D6E3D0D6E3D0D6E3D0D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2CF
        D4E2CFD4E2CED4E1CED4E1CED4E10000E0E5EFE0E5EFE0E4EFE0E4EFDFE4EFDF
        E4EFDFE3EEDEE3EEDEE3EEDEE3EEDEE3EDDDE2EDDDE2EDDDE2EDDDE1EDDCE1EC
        DCE1ECDCE1ECDCE0ECDBE0ECDBE0ECDBE0EBDBE0EBDADFEBDADFEBDADFEADADE
        EAD9DEEAD9DEEAD9DEEAD9DDE9D8DDE9D8DDE9D8DDE9D7DDE9D7DCE8D7DCE8D7
        DCE8D7DCE8D6DBE8D6DBE7D6DBE7D6DBE7D5DAE7D5DAE7D5DAE6D4DAE6D4DAE6
        D4D9E6D4D9E6D4D9E6D3D9E5D3D8E5D3D8E5D3D8E5D2D8E5D2D7E4D2D7E4D2D7
        E4D2D7E4D1D7E4D1D7E4D1D6E3D1D6E3D0D6E3D0D6E3D0D5E3D0D5E3D0D5E3D0
        D5E2CFD5E2CFD4E2CFD4E2CFD4E2CFD4E2CED4E1CED4E1CED4E10000E0E5EFE0
        E5EFE0E4EFDFE4EFDFE4EFDFE3EEDFE3EEDEE3EEDEE3EEDEE3EEDDE2EDDDE2ED
        DDE2EDDDE2EDDDE1ECDCE1ECDCE1ECDCE0ECDBE0ECDBE0ECDBE0EBDBE0EBDADF
        EBDADFEBDADFEBDADFEADADEEAD9DEEAD9DEEAD9DEE9D8DDE9D8DDE9D8DDE9D8
        DDE9D7DCE9D7DCE8D7DCE8D7DCE8D6DBE8D6DBE8D6DBE7D6DBE7D5DAE7D5DAE7
        D5DAE6D5DAE6D4DAE6D4D9E6D4D9E6D4D9E6D4D9E6D3D8E5D3D8E5D3D8E5D3D8
        E5D2D7E5D2D7E4D2D7E4D2D7E4D1D7E4D1D7E4D1D6E3D1D6E3D0D6E3D0D6E3D0
        D6E3D0D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2CFD4E2CFD4E2CED4E1CED4E1
        CED4E1CED3E10000E0E5EFE0E4EFE0E4EFDFE4EFDFE4EFDFE3EEDEE3EEDEE3EE
        DEE3EEDEE3EDDDE2EDDDE2EDDDE2EDDDE1EDDCE1ECDCE1ECDCE1ECDCE0ECDBE0
        ECDBE0ECDBE0EBDBE0EBDADFEBDADFEBDADFEADADEEAD9DEEAD9DEEAD9DEEAD9
        DDE9D8DDE9D8DDE9D8DDE9D7DDE99FA2A98081847071729FA2A9D6DBE8D6DBE7
        D6DBE7D6DBE7D5DAE7D5DAE7D5DAE6D4DAE6D4DAE6D4D9E6D4D9E6D4D9E6D3D9
        E5D3D8E5D3D8E5D3D8E5D2D8E5D2D7E4D2D7E4D2D7E4D2D7E4D1D7E4D1D7E4D1
        D6E3D1D6E3D0D6E3D0D6E3D0D5E3D0D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2
        CFD4E2CED4E2CED4E1CED4E1CED3E1CED3E10000E0E5EFE0E4EFDFE4EFDFE4EF
        DFE3EEDFE3EEDEE3EEDEE3EEDEE3EEDDE2EDDDE2EDDDE2EDDDE2EDDDE1ECDCE1
        ECDCE1ECDCE0ECDBE0ECDBE0ECDBE0EBDBE0EBDADFEBDADFEBDADFEBDADFEAD9
        DEEAD9DEEAD9DEEAD9DDE9D8DDE9D8DDE9D8DDE9D8DDE9D7DCE9B7BBC4606060
        6060609FA2A9D6DBE8D6DBE7D6DBE7D5DAE7D5DAE7D5DAE6D5DAE6D4DAE6D4D9
        E6D4D9E6D4D9E6D4D9E6D3D8E5D3D8E5D3D8E5D3D8E5D2D7E5D2D7E4D2D7E4D2
        D7E4D1D7E4D1D7E4D1D6E3D1D6E3D0D6E3D0D6E3D0D6E3D0D5E3D0D5E3D0D5E2
        CFD5E2CFD4E2CFD4E2CFD4E2CFD4E2CED4E1CED4E1CED4E1CED3E1CED3E10000
        E0E4EFE0E4EFDFE4EFDFE4EFDFE3EEDEE3EEDEE3EEDEE3EEDEE3EDDDE2EDDDE2
        EDDDE2EDDDE1EDDCE1ECDCE1ECDCE1ECDCE0ECDBE0ECDBE0ECDBE0EBDADFEBDA
        DFEBDADFEBDADFEADADEEAD9DEEAD9DEEAD9DEEAD9DDE9D8DDE9D8DDE9D8DDE9
        D7DDE9D7DCE8D7DCE8686869606060A7AAB2D6DBE7D6DBE7D6DBE7D5DAE7D5DA
        E7D5DAE6D4DAE6D4DAE6D4D9E6D4D9E6D4D9E6D3D9E5D3D8E5D3D8E5D3D8E5D2
        D8E5D2D7E4D2D7E4D2D7E4D2D7E4D1D7E4D1D7E4D1D6E3D1D6E3D0D6E3D0D6E3
        D0D5E3D0D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2CFD4E2CED4E2CED4E1CED4
        E1CED3E1CED3E1CED3E10000E0E4EFDFE4EFDFE4EFDFE3EEDFE3EEDEE3EEDEE3
        EEDEE3EEDDE2EDDDE2EDDDE2EDDDE2EDDDE1ECDCE1ECDCE1ECDCE0ECDBE0ECDB
        E0ECDBE0EBDBE0EBDADFEBDADFEBDADFEBDADFEAD9DEEAD9DEEAD9DEEAD9DDE9
        D8DDE9D8DDE9D8DDE9D8DDE9D7DCE9D7DCE8C7CBD6606060606060C6CBD6D6DB
        E7D6DBE7D5DAE7D5DAE7D5DAE6D5DAE6D4DAE6D4D9E6D4D9E6D4D9E6D4D9E6D3
        D8E5D3D8E5D3D8E5D3D8E5D2D7E5D2D7E4D2D7E4D2D7E4D1D7E4D1D7E4D1D6E3
        D1D6E3D0D6E3D0D6E3D0D6E3D0D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2CFD4
        E2CFD4E2CED4E1CED4E1CED4E1CED3E1CED3E1CDD3E10000E0E4EFDFE4EFDFE4
        EFACAFB59294998A8C8F606060606060797A7CC4C8D1DDE2EDDDE1EDDCE1ECDC
        E1ECDCE1ECDCE0ECB2B5BDDBE0ECDBE0EBDADFEBDADFEBB9BDC6B9BDC5DADEEA
        D9DEEAD9DEEAC1C5CED9DDE9D8DDE9D8DDE9D8DDE9BFC4CEC7CBD6CFD4DF9FA2
        A960606060606097999FD6DBE7D6DBE787898D97999FD5DAE6D4DAE6D4DAE6D4
        D9E6D4D9E6D4D9E6D3D9E5D3D8E5D3D8E5D3D8E5D2D8E5D2D7E4D2D7E4D2D7E4
        D2D7E4D1D7E4D1D7E4D1D6E3D1D6E3D0D6E3BABEC9D0D5E3D0D5E3D0D5E3D0D5
        E2CFD5E2CFD4E2CFD4E2CFD4E2CED4E2CED4E1CED4E1CED3E1CED3E1CED3E1CD
        D3E10000DFE4EFDFE4EF9B9DA260606060606068696960606060606060606060
        6060929498DDE1ECDCE1ECDCE1ECDCE0EC818285606060606060B2B5BDCACED8
        A1A4AA606060818285C9CDD8606060606060A1A3A9D8DDE9D8DDE9C0C4CE8081
        85606060606060606060AFB3BB78797B606060B7BAC368686960606060606060
        60607F8184D4DAE6D4D9E6B5B9C26F707296989F77787B96989E96989E77787B
        A4A7B0D2D7E4D2D7E4686869606060686869C9CEDAD1D6E3D0D6E39C9FA66060
        606F7071D0D5E3D0D5E2CFD5E27E7F837E7F8394969D94969D7D7F827D7F82CE
        D4E1CED3E1CED3E1CDD3E1CDD3E10000DFE4EFDFE4EFD7DAE5717173606060A3
        A6ACA3A5ABB3B7BE8A8B8F606060606060A2A5ABDCE1EC686969606060606060
        60606060606081828581828560606060606078797CD9DEEA8082856060608081
        85D0D5E0D8DDE98081856060606060606060609FA2A9D7DCE887898D60606068
        6869606060606060606060606060606060606060BDC1CBD4D9E67F8084606060
        6060606060606060606060606060608E9095D2D7E486888C606060606060C9CE
        DAD1D6E3D0D6E3A3A7AF606060606060BABEC9D0D5E2CFD5E27E7F8360606060
        60607D7F836060606060606F6F71A2A5ADCED3E1CDD3E1CDD3E00000DFE4EFDF
        E3EEDFE3EEB4B7BFBCC0C8DEE3EEDDE2EDDDE2EDD5D9E4606060606060686969
        A2A5AB606060606060606060C2C6CFDBE0EBDADFEBA1A4AA606060606060A1A3
        AAC1C5CE606060606060686869D8DDE9888A8E606060606060606060A7AAB2D7
        DCE88F919660606060606060606097999FD5DAE79EA1A78F9196606060606060
        96989FD4D9E696989F6060609DA0A7D3D8E5BCC0CA6060606F70729D9FA6D2D7
        E495989E60606077787AD1D6E3D0D6E3D0D6E376787A6060609C9EA6D0D5E2CF
        D5E2CFD4E27E7F83606060606060B1B5BFC7CCD876777A606060606060BEC4D0
        CDD3E1CDD3E00000DFE4EFDFE3EEDEE3EEDEE3EEDEE3EEDEE2EDDDE2EDDDE2ED
        B3B7BE606060606060818285A2A5AB606060606060797A7CDBE0EBDADFEBDADF
        EBD2D7E2686869606060A1A3AAD9DEEA80828560606068686978797B60606060
        6060606060A7AAB2D7DCE8D7DCE8A7AAB26060606060609FA2A8D5DAE7D5DAE7
        D5DAE6D4DAE67F8184606060686869BDC1CB7F8083606060CBD0DCD3D8E58E90
        95686869606060B4B7C1CACFDB6F7072606060686869C2C6D2D0D6E3D0D6E376
        777A606060ABAEB7D0D5E2CFD5E2CFD4E285878B606060606060B1B5BFA9ADB6
        676869606060676869CDD3E1CDD3E0CDD3E00000DFE3EEDFE3EEDEE3EEDEE3EE
        DEE3EEA3A5AB9A9DA2929498797A7C606060606060686969A2A4AB6060606869
        69CBCFD8DBE0EBDADFEBDADFEBDADFEB78797C606060989BA0D1D6E178797B60
        60606060606060606060606060609FA2A9D7DCE8D7DCE8D6DBE89FA2A9606060
        606060A6A9B1D5DAE7D5DAE6D5DAE6D4DAE6A6A9B06060609EA1A7ADB1B96060
        60606060606060606060606060606060606060C3C7D2C9CFDB6F70726060607E
        7F83D0D6E3D0D6E3D0D6E376777A6060607E7F83CFD5E2CFD4E2CFD4E2A3A6AE
        6060606060606060606060606060606060608C8E94CDD3E1CDD3E0CDD3E00000
        DFE3EEDEE3EEDEE3EEDEE3EECDD1DA717173797A7C6060606060606060609A9C
        A1C3C7D09A9CA1606060606060606060999BA1DADFEBDADFEBD2D7E168686960
        6060888A8ED9DEE9808185606060888A8E989AA070717260606088898DD7DCE8
        D7DCE8D6DBE88F91966060606060609EA1A8D5DAE7D5DAE6D4DAE69EA1A76060
        6060606087888DC4C9D36060606060608E9095D2D8E586888C7E8083BBBFCAD2
        D7E4D1D7E48D8F956060606F7071D0D6E3D0D6E3C9CDDA6F7071606060606060
        B1B6BFCFD4E2CFD4E29B9EA560606060606060606067686960606076777AB7BC
        C7CDD3E0CDD3E0CDD2E00000DFE3EEDEE3EEDEE3EEDEE3EED5D9E48A8B8F8183
        86818386717173818285B3B6BDDCE0ECBABEC7606060606060606060606060D2
        D7E2DADFEA919397606060606060989BA0D1D5E0686869606060888A8ED8DDE9
        707172606060606060989A9FD6DBE8D6DBE897999F6060606060606060609EA1
        A7D5DAE69EA1A777787B60606077787B9EA1A7B4B8C26060606060607F8083D2
        D7E5D2D7E4D2D7E4D2D7E4D1D7E4D1D7E49C9FA660606076787AD0D6E3A3A7AF
        94979D6768696060606060606060606060609B9EA594969D606060606060B8BD
        C7CED3E1CED3E1CDD3E1CDD3E1CDD3E0CDD3E0CDD2E00000DEE3EEDEE3EEDEE3
        EEDEE2EDDDE2EDDDE2EDDDE2EDDDE1ED818285898B8FDCE1ECDCE0ECDBE0EC99
        9CA1606060606060606060999BA1B1B5BC606060606060686869D1D6E0D9DDE9
        A8ABB2606060888A8ED7DDE9D7DCE8909296606060707172C6CBD6D6DBE7C6CB
        D56868696060607070726060606060606060606060606060609EA1A7D3D9E5CB
        D0DC60606060606095989ED2D7E4D2D7E4D2D7E4D2D7E4D1D7E4D1D6E486878C
        60606085878CC9CEDA76777A60606060606060606060606060606094969D9496
        9D93969D606060606060A9ADB6CED3E1CED3E1CDD3E1CDD3E0CDD3E0CDD2E0CD
        D2E00000DEE3EEDEE3EEDEE3EEDDE2EDD5D9E4DDE2EDDDE2EDDDE1EC68696968
        6969B3B5BDDBE0ECDBE0ECDBE0EBCBCFD8818285606060898A8E606060707172
        606060B9BCC5D9DDE9D0D5E0808185A0A3A9D8DDE9D7DCE9D7DCE8CFD4DF7879
        7B6060609FA2A9D6DBE7CED3DE7F818497999FAEB1B98F91966F70726868697F
        808496989FCCD1DDD3D8E5C4C8D360606060606086888CD2D7E4D2D7E4D2D7E4
        D1D7E4D1D7E4C2C6D26F7071606060676869D0D6E3D0D5E3B2B6C0ABAEB7C0C5
        D1C0C5D19B9EA5CFD4E2CFD4E276777A606060606060BFC4D0CED3E1CDD3E1CD
        D3E1CDD3E0CDD3E0CDD2E0CDD2E00000DEE3EEDEE3EEDEE2ED92949860606081
        8386B3B6BE9A9CA1606060606060606060AAADB4DBE0EBDBE0EBDADFEBDADFEB
        DADFEBDADFEADADEEAD9DEEAD9DEEAD9DEE9D9DDE9D8DDE9D8DDE9D8DDE9D7DD
        E9D7DCE8D7DCE8D7DCE8D7DCE8D6DBE8D6DBE7D6DBE7D6DBE7D5DAE7D5DAE7D5
        DAE6D4DAE6D4DAE6D4D9E6D4D9E6D4D9E6D3D9E5D3D8E5D3D8E5BCC0CAD2D8E5
        D2D7E4D2D7E4D2D7E4D2D7E4D1D7E4D1D6E4D1D6E3D1D6E3D0D6E3D0D6E3D0D5
        E3D0D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2CFD4E2CED4E2B1B5BF9B9EA5CE
        D3E1CED3E1CED3E1CDD3E1CDD3E0CDD3E0CDD2E0CDD2E0CDD2E00000DEE3EEDE
        E3EEDDE2ED929498606060606060606060606060606060606060686969D3D7E3
        DBE0EBDBE0EBDADFEBDADFEBDADFEADADFEAD9DEEAD9DEEAD9DEEAD9DDE9D8DD
        E9D8DDE9D8DDE9D8DDE9D7DCE9D7DCE8D7DCE8D7DCE8D6DBE8D6DBE8D6DBE7D6
        DBE7D5DAE7D5DAE7D5DAE6D5DAE6D4DAE6D4D9E6D4D9E6D4D9E6D4D9E6D3D8E5
        D3D8E5D3D8E5D3D8E5D2D7E5D2D7E4D2D7E4D2D7E4D1D7E4D1D7E4D1D6E3D1D6
        E3D0D6E3D0D6E3D0D6E3D0D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2CFD4E2CF
        D4E2CED4E1CED4E1CED4E1CED3E1CED3E1CDD3E1CDD3E1CDD3E0CDD3E0CDD2E0
        CDD2E0CDD2E00000DEE3EEDEE2EDD5D9E4818386606060606060606060606060
        606060606060B2B5BDDBE0EBDBE0EBDADFEBDADFEBDADFEBDADFEADADEEAD9DE
        EAD9DEEAD9DEE9D9DDE9D8DDE9D8DDE9D8DDE9D7DDE9D7DCE8D7DCE8D7DCE8D7
        DCE8D6DBE8D6DBE7D6DBE7D5DAE7D5DAE7D5DAE7D5DAE6D4DAE6D4DAE6D4D9E6
        D4D9E6D4D9E6D3D9E5D3D8E5D3D8E5D3D8E5D2D8E5D2D7E4D2D7E4D2D7E4D1D7
        E4D1D7E4D1D6E4D1D6E3D1D6E3D0D6E3D0D6E3D0D5E3D0D5E3D0D5E3D0D5E2CF
        D5E2CFD4E2CFD4E2CFD4E2CED4E2CED4E1CED4E1CED3E1CED3E1CDD3E1CDD3E1
        CDD3E0CDD3E0CDD2E0CDD2E0CDD2E0CDD2E00000DEE3EEDDE2EDDDE2EDDDE2ED
        DDE2EDDDE1ECA2A5AB717173A2A4ABAAADB4DBE0ECDBE0EBDBE0EBDADFEBDADF
        EBDADFEADADFEAD9DEEAD9DEEAD9DEEAD9DDE9D8DDE9D8DDE9D8DDE9D8DDE9D7
        DCE9D7DCE8D7DCE8D7DCE8D6DBE8D6DBE8D6DBE7D6DBE7D5DAE7D5DAE7D5DAE6
        D5DAE6D4DAE6D4D9E6D4D9E6D4D9E6D4D9E6D3D8E5D3D8E5D3D8E5D3D8E5D2D7
        E5D2D7E4D2D7E4D2D7E4D1D7E4D1D7E4D1D6E3D1D6E3D0D6E3D0D6E3D0D6E3D0
        D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2CFD4E2CFD4E2CED4E1CED4E1CED4E1
        CED3E1CED3E1CDD3E1CDD3E1CDD3E0CDD3E0CDD2E0CDD2E0CDD2E0CCD2E00000
        DEE2EDDDE2EDDDE2EDDDE2EDDDE1EDDCE1ECD4D8E39A9CA1DCE0ECDBE0ECDBE0
        EBDBE0EBDADFEBDADFEBDADFEBDADFEADADEEAD9DEEAD9DEEAD9DEE9D8DDE9D8
        DDE9D8DDE9D8DDE9D7DDE9D7DCE8D7DCE8D7DCE8D7DCE8D6DBE8D6DBE7D6DBE7
        D5DAE7D5DAE7D5DAE7D5DAE6D4DAE6D4D9E6D4D9E6D4D9E6D4D9E6D3D9E5D3D8
        E5D3D8E5D3D8E5D2D8E5D2D7E4D2D7E4D2D7E4D1D7E4D1D7E4D1D6E4D1D6E3D1
        D6E3D0D6E3D0D6E3D0D5E3D0D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2CFD4E2
        CED4E2CED4E1CED4E1CED3E1CED3E1CDD3E1CDD3E1CDD3E0CDD3E0CDD2E0CDD2
        E0CDD2E0CDD2E0CCD2E00000DDE2EDDDE2EDDDE2EDDDE2EDDDE1ECDCE1ECDCE1
        ECDCE0ECDBE0ECDBE0ECDBE0EBDBE0EBDADFEBDADFEBDADFEADADFEAD9DEEAD9
        DEEAD9DEEAD9DDE9D8DDE9D8DDE9D8DDE9D8DDE9D7DCE9D7DCE8D7DCE8D7DCE8
        D6DBE8D6DBE7D6DBE7D6DBE7D5DAE7D5DAE7D5DAE6D5DAE6D4DAE6D4D9E6D4D9
        E6D4D9E6D4D9E6D3D8E5D3D8E5D3D8E5D3D8E5D2D7E5D2D7E4D2D7E4D2D7E4D1
        D7E4D1D7E4D1D6E3D1D6E3D0D6E3D0D6E3D0D5E3D0D5E3D0D5E3D0D5E2CFD5E2
        CFD4E2CFD4E2CFD4E2CFD4E2CED4E1CED4E1CED4E1CED3E1CED3E1CDD3E1CDD3
        E1CDD3E0CDD2E0CDD2E0CDD2E0CDD2E0CCD2E0CCD2E00000DDE2EDDDE2EDDDE2
        EDDDE1EDDCE1ECDCE1ECDCE1ECDCE0ECDBE0ECDBE0EBDBE0EBDADFEBDADFEBDA
        DFEBDADFEADADEEAD9DEEAD9DEEAD9DEE9D8DDE9D8DDE9D8DDE9D8DDE9D7DDE9
        D7DCE8D7DCE8D7DCE8D7DBE8D6DBE8D6DBE7D6DBE7D5DAE7D5DAE7D5DAE7D5DA
        E6D4DAE6D4D9E6D4D9E6D4D9E6D4D9E6D3D9E5D3D8E5D3D8E5D3D8E5D2D8E5D2
        D7E4D2D7E4D2D7E4D1D7E4D1D7E4D1D6E3D1D6E3D1D6E3D0D6E3D0D6E3D0D5E3
        D0D5E3D0D5E3D0D5E2CFD5E2CFD4E2CFD4E2CFD4E2CED4E2CED4E1CED4E1CED3
        E1CED3E1CDD3E1CDD3E1CDD3E0CDD3E0CDD2E0CDD2E0CDD2E0CDD2E0CCD2E0CC
        D2E00000}
      Visible = False
      OnClick = ImgHighCloseClick
    end
  end
end
