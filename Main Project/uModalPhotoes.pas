unit uModalPhotoes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TGA, ExtCtrls;

type
  TFormModalPh = class(TForm)
    Image1: TImage;
    PanelClose: TPanel;
    ImgNormCls: TImage;
    ImgHighLCls: TImage;
    PanelArt: TPanel;
    ImgNormArt: TImage;
    ImgHighArt: TImage;
    PanelZepp: TPanel;
    ImgNormZ: TImage;
    ImgHighZ: TImage;
    PanelFleet: TPanel;
    ImgNormFl: TImage;
    ImgHighFl: TImage;
    PanelBattleField: TPanel;
    ImgNormBF: TImage;
    ImgHighBF: TImage;
    PanelTanks: TPanel;
    ImgNormTan: TImage;
    ImgHighTan: TImage;
    PanelAvia: TPanel;
    ImgNormAvia: TImage;
    ImgHighAvia: TImage;
    PanelGas: TPanel;
    ImgNormGas: TImage;
    ImgHighGas: TImage;
    PanelClose2: TPanel;
    ImgNormClose: TImage;
    ImgHighClose: TImage;
    PanelMisc: TPanel;
    ImgNormMisc: TImage;
    ImgHighMisc: TImage;
    PanelDistr: TPanel;
    ImgNormDistr: TImage;
    ImgHighDistr: TImage;
    PanelCities: TPanel;
    ImgNormCity: TImage;
    ImgHighCity: TImage;
    procedure ImgHighLClsClick(Sender: TObject);
    procedure ImgNormClsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgHighArtClick(Sender: TObject);
    procedure ImgHighZClick(Sender: TObject);
    procedure ImgHighFlClick(Sender: TObject);
    procedure ImgHighBFClick(Sender: TObject);
    procedure ImgHighTanClick(Sender: TObject);
    procedure ImgHighAviaClick(Sender: TObject);
    procedure ImgHighGasClick(Sender: TObject);
    procedure ImgNormArtMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormAviaMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormBFMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormFlMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormGasMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormTanMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormZMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ImgHighCloseClick(Sender: TObject);
    procedure ImgNormCloseMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgHighCityClick(Sender: TObject);
    procedure ImgHighDistrClick(Sender: TObject);
    procedure ImgHighMiscClick(Sender: TObject);
    procedure ImgNormMiscMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormDistrMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormCityMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
  end;

var
  FormModalPh: TFormModalPh;

implementation

uses uPhotoes;

{$R *.dfm}

procedure TFormModalPh.WMNCHitTest(var M: TWMNCHitTest);
begin
inherited;
if M.Result = htClient then M.Result := htCaption;
ImgHighLCls.Visible:=False;
ImgHighAvia.Visible:=False;
ImgHighBF.Visible:=False;
ImgHighFl.Visible:=False;
ImgHighGas.Visible:=False;
ImgHighArt.Visible:=False;
ImgHighTan.Visible:=False;
ImgHighMisc.Visible:=False;
ImgHighCity.Visible:=False;
ImgHighZ.Visible:=False;
ImgHighDistr.Visible:=False;
ImgHighClose.Visible:=False;
end;


//---- ������ ----

//������� [x]
procedure TFormModalPh.ImgHighLClsClick(Sender: TObject);
begin
Close;
end;

//����������
procedure TFormModalPh.ImgHighArtClick(Sender: TObject);
begin
FormPhotoes.MPath:=ExtractFilePath(ParamStr(0))+'\Data\Photoes\Artillery\Artillery';
FormPhotoes.MaxCount:=57;
FormPhotoes.ShowModal;
ImgHighArt.Visible:=False;
end;

//���������
procedure TFormModalPh.ImgHighZClick(Sender: TObject);
begin
FormPhotoes.MPath:=ExtractFilePath(ParamStr(0))+'\Data\Photoes\Zeppelins\Zeppelins';
FormPhotoes.MaxCount:=42;
FormPhotoes.ShowModal;
ImgHighZ.Visible:=False;
end;

//����
procedure TFormModalPh.ImgHighFlClick(Sender: TObject);
begin
FormPhotoes.MPath:=ExtractFilePath(ParamStr(0))+'\Data\Photoes\Fleet\Fleet';
FormPhotoes.MaxCount:=46;
FormPhotoes.ShowModal;
ImgHighFl.Visible:=False;
end;

//���� ���
procedure TFormModalPh.ImgHighBFClick(Sender: TObject);
begin
FormPhotoes.MPath:=ExtractFilePath(ParamStr(0))+'\Data\Photoes\Battlefield\Battlefield';
FormPhotoes.MaxCount:=52;
FormPhotoes.ShowModal;
ImgHighBF.Visible:=False;
end;

//�����
procedure TFormModalPh.ImgHighTanClick(Sender: TObject);
begin
FormPhotoes.MPath:=ExtractFilePath(ParamStr(0))+'\Data\Photoes\Tanks\Tanks';
FormPhotoes.MaxCount:=50;
FormPhotoes.ShowModal;
ImgHighTan.Visible:=False;
end;

//������
procedure TFormModalPh.ImgHighCityClick(Sender: TObject);
begin
FormPhotoes.MPath:=ExtractFilePath(ParamStr(0))+'\Data\Photoes\Cities\Cities';
FormPhotoes.MaxCount:=49;
FormPhotoes.ShowModal;
ImgHighCity.Visible:=False;
end;

//����������
procedure TFormModalPh.ImgHighDistrClick(Sender: TObject);
begin
FormPhotoes.MPath:=ExtractFilePath(ParamStr(0))+'\Data\Photoes\Distroy\Distroy';
FormPhotoes.MaxCount:=41;
FormPhotoes.ShowModal;
ImgHighDistr.Visible:=False;
end;

//������
procedure TFormModalPh.ImgHighMiscClick(Sender: TObject);
begin
FormPhotoes.MPath:=ExtractFilePath(ParamStr(0))+'\Data\Photoes\Misc\Misc';
FormPhotoes.MaxCount:=50;
FormPhotoes.ShowModal;
ImgHighMisc.Visible:=False;
end;

//��������� ���
procedure TFormModalPh.ImgHighAviaClick(Sender: TObject);
begin
FormPhotoes.MPath:=ExtractFilePath(ParamStr(0))+'\Data\Photoes\Avia\Avia';
FormPhotoes.MaxCount:=37;
FormPhotoes.ShowModal;
ImgHighAvia.Visible:=False;
end;

//���������� ������
procedure TFormModalPh.ImgHighGasClick(Sender: TObject);
begin
FormPhotoes.MPath:=ExtractFilePath(ParamStr(0))+'\Data\Photoes\Gas\Gas';
FormPhotoes.MaxCount:=21;
FormPhotoes.ShowModal;
ImgHighGas.Visible:=False;
end;

//�������
procedure TFormModalPh.ImgHighCloseClick(Sender: TObject);
begin
Close;
end;

//---- ����� ������ ----


//---- ��������� ----

procedure TFormModalPh.ImgNormClsMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighLCls.Visible:=True;
end;

procedure TFormModalPh.ImgNormArtMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighArt.Visible:=True;
end;

procedure TFormModalPh.ImgNormAviaMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighAvia.Visible:=True;
end;

procedure TFormModalPh.ImgNormBFMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighBF.Visible:=True;
end;

procedure TFormModalPh.ImgNormFlMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighFl.Visible:=True;
end;

procedure TFormModalPh.ImgNormGasMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighGas.Visible:=True;
end;

procedure TFormModalPh.ImgNormTanMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighTan.Visible:=True;
end;

procedure TFormModalPh.ImgNormZMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighZ.Visible:=True;
end;

procedure TFormModalPh.ImgNormCityMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighCity.Visible:=True;
end;

procedure TFormModalPh.ImgNormDistrMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighDistr.Visible:=True;
end;

procedure TFormModalPh.ImgNormMiscMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighMisc.Visible:=True;
end;

procedure TFormModalPh.ImgNormCloseMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighClose.Visible:=True;
end;

//---- ����� ��������� ----


procedure TFormModalPh.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
case Key of
  49,97: ImgHighArtClick(ImgHighArt);
  50,98: ImgHighZClick(ImgHighZ);
  51,99: ImgHighFlClick(ImgHighFl);
  52,100: ImgHighBFClick(ImgHighBF);
  53,101: ImgHighTanClick(ImgHighTan);
  54,102: ImgHighCityClick(ImgHighTan);
  55,103: ImgHighDistrClick(ImgHighTan);
  56,104: ImgHighMiscClick(ImgHighTan);
  57,105: ImgHighAviaClick(ImgHighAvia);
  48,96: ImgHighGasClick(ImgHighGas);
  end;
end;

procedure TFormModalPh.FormShow(Sender: TObject);
begin
ImgHighLCls.Visible:=False;
ImgHighAvia.Visible:=False;
ImgHighBF.Visible:=False;
ImgHighFl.Visible:=False;
ImgHighGas.Visible:=False;
ImgHighArt.Visible:=False;
ImgHighTan.Visible:=False;
ImgHighMisc.Visible:=False;
ImgHighCity.Visible:=False;
ImgHighZ.Visible:=False;
ImgHighDistr.Visible:=False;
ImgHighClose.Visible:=False;
end;

procedure TFormModalPh.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = 27 then Close;
end;

end.
