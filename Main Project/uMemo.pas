unit uMemo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, TGA;

type
  TFormMemo = class(TForm)
    Memo1: TMemo;
    Image1: TImage;
    PanelClose: TPanel;
    ImgNormCls: TImage;
    ImgHighLCls: TImage;
    PanelClose2: TPanel;
    ImgNormClose: TImage;
    ImgHighClose: TImage;
    procedure ImgHighCloseClick(Sender: TObject);
    procedure ImgHighLClsClick(Sender: TObject);
    procedure ImgNormCloseMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormClsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Memo1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
  public
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
  end;

var
  FormMemo: TFormMemo;

implementation

{$R *.dfm}

procedure TFormMemo.WMNCHitTest(var M: TWMNCHitTest);
begin
inherited;
if M.Result = htClient then M.Result := htCaption;
ImgHighLCls.Visible:=False;
ImgHighClose.Visible:=False;
end;

//---- ������ ----

//������� [x]
procedure TFormMemo.ImgHighLClsClick(Sender: TObject);
begin
Close;
end;

//�������
procedure TFormMemo.ImgHighCloseClick(Sender: TObject);
begin
Close;
end;

//---- ����� ������ ----


//---- ��������� ----

procedure TFormMemo.ImgNormClsMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
ImgHighLCls.Visible:=True;
end;

procedure TFormMemo.ImgNormCloseMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
ImgHighClose.Visible:=True;
end;

//---- ����� ��������� ----


procedure TFormMemo.Memo1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = 27 then Close;
end;

procedure TFormMemo.FormShow(Sender: TObject);
begin
ImgHighLCls.Visible:=False;
ImgHighClose.Visible:=False;
end;

procedure TFormMemo.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = 27 then Close;
end;

end.
