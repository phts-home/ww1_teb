unit uInform;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, TGA;

type
  TFormInf = class(TForm)
    Image1: TImage;
    PanelHrono: TPanel;
    ImgNormHr: TImage;
    ImgHighLHr: TImage;
    PanelClose: TPanel;
    ImgNormCls: TImage;
    ImgHighLCls: TImage;
    PanelStat: TPanel;
    ImgNormSt: TImage;
    ImgHighLSt: TImage;
    PanelPhoto: TPanel;
    ImgNormPh: TImage;
    ImgHighLPh: TImage;
    PanelClose2: TPanel;
    ImgNormClose: TImage;
    ImgHighClose: TImage;
    PanelVideo: TPanel;
    ImgNormVid: TImage;
    ImgHighVid: TImage;
    procedure ImgHighLClsClick(Sender: TObject);
    procedure ImgNormClsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgHighLHrClick(Sender: TObject);
    procedure ImgNormHrMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgHighLStClick(Sender: TObject);
    procedure ImgNormStMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormPhMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgHighLPhClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ImgHighCloseClick(Sender: TObject);
    procedure ImgNormCloseMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgHighVidClick(Sender: TObject);
    procedure ImgNormVidMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
  public
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
  end;

var
  FormInf: TFormInf;

implementation

uses uModalPhotoes, uStatistics, uHrono, uModalVideos;

{$R *.dfm}

procedure TFormInf.WMNCHitTest(var M: TWMNCHitTest);
begin
inherited;
if M.Result = htClient then M.Result := htCaption;
ImgHighLCls.Visible:=False;
ImgHighLHr.Visible:=False;
ImgHighLSt.Visible:=False;
ImgHighLPh.Visible:=False;
ImgHighVid.Visible:=False;
ImgHighClose.Visible:=False;
end;


//---- ������ ----

//������� [x]
procedure TFormInf.ImgHighLClsClick(Sender: TObject);
begin
Close;
end;

//���������� �������
procedure TFormInf.ImgHighLHrClick(Sender: TObject);
begin
ImgHighLHr.Visible:=False;
FormHrono.ShowModal;
end;

//���������� �����
procedure TFormInf.ImgHighLStClick(Sender: TObject);
begin
ImgHighLSt.Visible:=False;
FormStat.ShowModal;
end;

//����������
procedure TFormInf.ImgHighLPhClick(Sender: TObject);
begin
ImgHighLPh.Visible:=False;
FormModalPh.ShowModal;
end;

//�����
procedure TFormInf.ImgHighVidClick(Sender: TObject);
begin
ImgHighVid.Visible:=False;
FormModalVid.ShowModal;
end;

//�������
procedure TFormInf.ImgHighCloseClick(Sender: TObject);
begin
Close;
end;

//---- ����� ������ ----


//---- ��������� ----

procedure TFormInf.ImgNormClsMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
ImgHighLCls.Visible:=True;
end;

procedure TFormInf.ImgNormHrMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
ImgHighLHr.Visible:=True;
end;

procedure TFormInf.ImgNormStMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
ImgHighLSt.Visible:=True;
end;

procedure TFormInf.ImgNormPhMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
ImgHighLPh.Visible:=True;
end;

procedure TFormInf.ImgNormVidMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
ImgHighVid.Visible:=True;
end;

procedure TFormInf.ImgNormCloseMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighClose.Visible:=True;
end;

//---- ����� ��������� ----


procedure TFormInf.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
case Key of
  49,97: ImgHighLHrClick(ImgHighLHr);
  50,98: ImgHighLStClick(ImgHighLSt);
  51,99: ImgHighLPhClick(ImgHighLPh);
  52,100:ImgHighVidClick(ImgHighVid);
  end;
end;

procedure TFormInf.FormShow(Sender: TObject);
begin
ImgHighLCls.Visible:=False;
ImgHighLHr.Visible:=False;
ImgHighLSt.Visible:=False;
ImgHighLPh.Visible:=False;
ImgHighVid.Visible:=False;
ImgHighClose.Visible:=False;
end;

procedure TFormInf.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = 27 then Close;
end;

end.
