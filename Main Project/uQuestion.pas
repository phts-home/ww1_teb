unit uQuestion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, TGA;

type
  TFormQuest = class(TForm)
    Image1: TImage;
    PanelClose: TPanel;
    ImgNormCls: TImage;
    ImgHighLCls: TImage;
    PanelYes: TPanel;
    ImgNormYes: TImage;
    ImgHighYes: TImage;
    PanelNo: TPanel;
    ImgNormNo: TImage;
    ImgHighNo: TImage;
    procedure ImgHighLClsClick(Sender: TObject);
    procedure ImgHighYesClick(Sender: TObject);
    procedure ImgHighNoClick(Sender: TObject);
    procedure ImgNormClsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormNoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ImgNormYesMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormNoMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
  public
    Result: Boolean;
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
  end;

var
  FormQuest: TFormQuest;

implementation

{$R *.dfm}

procedure TFormQuest.WMNCHitTest(var M: TWMNCHitTest);
begin
inherited;
if M.Result = htClient then M.Result := htCaption;
ImgHighLCls.Visible:=False;
ImgHighYes.Visible:=False;
ImgHighNo.Visible:=False;
end;

//---- ������ ----

//������� [x]
procedure TFormQuest.ImgHighLClsClick(Sender: TObject);
begin
Result:=False;
Close;
end;

//��
procedure TFormQuest.ImgHighYesClick(Sender: TObject);
begin
Result:=True;
Close;
end;

//���
procedure TFormQuest.ImgHighNoClick(Sender: TObject);
begin
Result:=False;
Close;
end;

//---- ����� ������ ----


//---- ��������� ----

procedure TFormQuest.ImgNormClsMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
ImgHighLCls.Visible:=True;
end;

procedure TFormQuest.ImgNormYesMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighYes.Visible:=True;
end;

procedure TFormQuest.ImgNormNoMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighNo.Visible:=True;
end;

procedure TFormQuest.ImgNormNoClick(Sender: TObject);
begin

end;


//---- ����� ��������� ----


procedure TFormQuest.FormShow(Sender: TObject);
begin
Result:=False;
ImgHighLCls.Visible:=False;
ImgHighYes.Visible:=False;
ImgHighNo.Visible:=False;
end;

procedure TFormQuest.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if(Key = 13)or(Key = 32)then ImgHighYesClick(ImgHighYes);
end;

procedure TFormQuest.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = 27 then ImgHighNoClick(ImgHighNo);
end;

end.
