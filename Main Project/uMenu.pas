unit uMenu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TGA, ExtCtrls, StdCtrls, ComCtrls, CommCtrl, Gauges,
  OleCtrls, ShockwaveFlashObjects_TLB;

type
  TFormMenu = class(TForm)
    ImgLeftBorder: TImage;
    ImgTopBorder: TImage;
    ImgRightBorder: TImage;
    ImgBottomBorder: TImage;
    ImgRightBorder1: TImage;
    ImgLeftBorder1: TImage;
    PanelPlay: TPanel;
    ImgNormPlay: TImage;
    ImgHighPlay: TImage;
    PanelPause: TPanel;
    ImgNormPause: TImage;
    ImgHighPause: TImage;
    PanelStop: TPanel;
    ImgNormStop: TImage;
    ImgHighStop: TImage;
    ImgScreen: TImage;
    TimerScreen: TTimer;
    ImgBackgr: TImage;
    Gauge1: TGauge;
    MainMenu: TPanel;
    Image1: TImage;
    PanelInform: TPanel;
    ImgNormInf: TImage;
    ImgHighInf: TImage;
    PanelClose: TPanel;
    ImgNormClose: TImage;
    ImgHighClose: TImage;
    PanelAbout: TPanel;
    ImgNormAb: TImage;
    ImgHighAb: TImage;
    PanelMenu: TPanel;
    ImgNormMenu: TImage;
    ImgHighMenu: TImage;
    ShockwaveFlash1: TShockwaveFlash;
    Label1: TLabel;
    TimerWatch: TTimer;
    Panel1: TPanel;
    Image2: TImage;
    Panel2: TPanel;
    Image3: TImage;
    LabelVer: TLabel;
    procedure ImgHighInfClick(Sender: TObject);
    procedure ImgHighAbClick(Sender: TObject);
    procedure ImgHighCloseClick(Sender: TObject);
    procedure ImgNormAbMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormCloseMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormInfMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormMenuMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure ImgHighMenuMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ImgNormPlayMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ImgNormStopMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ImgHighPlayMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure TimerScreenTimer(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure TimerWatchTimer(Sender: TObject);
    procedure ImgHighPauseMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ImgNormPauseMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image3MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
  public
    MainMenuShowing, Pause, Playing, HidingAll, AltPause: Boolean;
    procedure StartProc;
    procedure EndProc;
    procedure PauseProc(Value: Boolean);
    procedure SetVisibleAll(Value: Boolean);
    procedure SetVisibleMainMenu(Value: Boolean);
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
    function FormatTime(Frames: Integer): String;
  end;

var
  FormMenu: TFormMenu;

implementation

uses uQuestion, uInform, uAbout;

{$R *.dfm}

procedure TFormMenu.WMNCHitTest(var M: TWMNCHitTest);
begin
inherited;
ImgHighMenu.Visible:=False;
end;


procedure TFormMenu.SetVisibleMainMenu(Value: Boolean);
begin
if Value then
  begin
  if Pause then
    begin
    AltPause:=True;
    end else
    begin
    AltPause:=False;
    PauseProc(True);
    end;
  end else
  if not AltPause then PauseProc(False);

MainMenuShowing:=Value;
MainMenu.Visible:=Value;
end;

procedure TFormMenu.SetVisibleAll(Value: Boolean);
begin
if MainMenuShowing and Value then
  begin
  SetVisibleMainMenu(Value);
  end;

if not Value then
  begin
  SetVisibleMainMenu(Value);
  end;

Label1.Visible:=Value;
Panel1.Visible:=Value;
Panel2.Visible:=Value;

HidingAll:=not Value;

ShockwaveFlash1.Visible:=Value;

PanelPause.Visible:=Value;
PanelPlay.Visible:=Value;
PanelStop.Visible:=Value;
PanelMenu.Visible:=Value;
end;

procedure TFormMenu.EndProc;
begin
MainMenuShowing:=False;
MainMenu.Visible:=False;
//����� ��������
ShockwaveFlash1.Stop;
ShockwaveFlash1.Rewind;

Pause:=False;
Playing:=False;
ImgHighStop.Visible:=True;
ImgHighPlay.Visible:=False;
ImgHighPause.Visible:=False;
end;

procedure TFormMenu.StartProc;
begin
MainMenuShowing:=False;
MainMenu.Visible:=False;
//������ ��������
ShockwaveFlash1.Stop;
ShockwaveFlash1.Play;

Playing:=True;
Pause:=False;
ImgHighPlay.Visible:=True;
ImgHighStop.Visible:=False;
end;

procedure TFormMenu.PauseProc(Value: Boolean);
begin
if Playing then
  begin
  Pause:=Value;
  MainMenuShowing:=False;
  MainMenu.Visible:=False;
  if Value then
    begin
    //�����
    ShockwaveFlash1.Stop;
    end else
    begin
    //�����������
    ShockwaveFlash1.Play;
    end;
  ImgNormPause.Visible:=not Value;
  ImgHighPause.Visible:=Value;
  end;
end;

function TFormMenu.FormatTime(Frames: Integer): String;
var Sec, Min: Integer;
begin
Sec:=Frames div 25;
Min:=Sec div 60;
Sec:=Sec - Min * 60;
if Length(IntToStr(Sec)) = 1
  then FormatTime:=IntToStr(Min) + ':0'+IntToStr(Sec)
  else FormatTime:=IntToStr(Min) + ':'+IntToStr(Sec);
end;

//---- ������ ----

//������� ����
procedure TFormMenu.ImgHighMenuMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
SetVisibleMainMenu(not MainMenuShowing);
end;

//�������
procedure TFormMenu.ImgHighInfClick(Sender: TObject);
begin
ImgHighInf.Visible:=False;
SetVisibleMainMenu(True);
FormInf.ShowModal;
end;

//� ���������
procedure TFormMenu.ImgHighAbClick(Sender: TObject);
begin
ImgHighAb.Visible:=False;
SetVisibleMainMenu(True);
FormAbout.ShowModal;
end;

//�����
procedure TFormMenu.ImgHighCloseClick(Sender: TObject);
begin
Close;
end;

//Play
procedure TFormMenu.ImgHighPlayMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if Pause then
  begin
  PauseProc(False);
  MainMenuShowing:=False;
  MainMenu.Visible:=False;
  end;
end;

procedure TFormMenu.ImgNormPlayMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
StartProc;
MainMenuShowing:=False;
MainMenu.Visible:=False;
end;

//Pause
procedure TFormMenu.ImgHighPauseMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
MainMenuShowing:=False;
MainMenu.Visible:=False;
PauseProc(False);
end;

procedure TFormMenu.ImgNormPauseMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
MainMenuShowing:=False;
MainMenu.Visible:=False;
PauseProc(True);
end;

//Stop
procedure TFormMenu.ImgNormStopMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
if Playing then
  begin
  EndProc;
  SetVisibleMainMenu(False);
  end;
end;

//Rewind
procedure TFormMenu.Image2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var SomeBool: Boolean;
begin
if Playing then ShockwaveFlash1.Stop;
ShockwaveFlash1.GotoFrame(ShockwaveFlash1.CurrentFrame-250);
SomeBool:=False;
if Playing then
  begin
  SomeBool:=Pause;
  end;
MainMenuShowing:=False;
MainMenu.Visible:=False;
PauseProc(SomeBool);
end;

//Forward
procedure TFormMenu.Image3MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var SomeBool: Boolean;
begin
if Playing then ShockwaveFlash1.Stop;
ShockwaveFlash1.GotoFrame(ShockwaveFlash1.CurrentFrame+250);
SomeBool:=False;
if Playing then
  begin
  SomeBool:=Pause;
  end;
MainMenuShowing:=False;
MainMenu.Visible:=False;
PauseProc(SomeBool);
end;

//----  ����� ������  ----

procedure TFormMenu.Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
ImgHighAb.Visible:=False;
ImgHighClose.Visible:=False;
ImgHighInf.Visible:=False;
ImgHighMenu.Visible:=False;
end;

procedure TFormMenu.ImgNormAbMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
ImgHighAb.Visible:=True;
end;

procedure TFormMenu.ImgNormCloseMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighClose.Visible:=True;
end;

procedure TFormMenu.ImgNormInfMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighInf.Visible:=True;
end;

procedure TFormMenu.ImgNormMenuMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighMenu.Visible:=True;
end;

procedure TFormMenu.FormCreate(Sender: TObject);
var CurrentFile: String;
begin
Randomize;

SetVisibleAll(False);
try
  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Data\Map\Map000.swf';
  ShockwaveFlash1.Movie:=CurrentFile;

  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Textures\Interface\Screen000.tga';
  ImgScreen.Picture.LoadFromFile(CurrentFile);

  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Textures\Interface\MainMenu000.tga';
  Image1.Picture.LoadFromFile(CurrentFile);

  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Textures\Interface\Frame_TopBorder000.tga';
  ImgTopBorder.Picture.LoadFromFile(CurrentFile);
  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Textures\Interface\Frame_LeftBorder000.tga';
  ImgLeftBorder.Picture.LoadFromFile(CurrentFile);
  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Textures\Interface\Frame_RightBorder000.tga';
  ImgRightBorder.Picture.LoadFromFile(CurrentFile);
  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Textures\Interface\Frame_BottomBorder000.tga';
  ImgBottomBorder.Picture.LoadFromFile(CurrentFile);

  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Textures\Interface\Frame_LeftBorder001.tga';
  ImgLeftBorder1.Picture.LoadFromFile(CurrentFile);
  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Textures\Interface\Frame_RightBorder001.tga';
  ImgRightBorder1.Picture.LoadFromFile(CurrentFile);
  except
    Application.MessageBox(PChar('������ ��� ������ �����'+#13+CurrentFile
            +#13#13+'���������� ������ ��������� ����������'),
            PChar(FormMenu.Caption),MB_ICONERROR+mb_OK);
    Application.Terminate;
    end;

ShockwaveFlash1.BGColor:='#000000';
PauseProc(False);
EndProc;
SetVisibleMainMenu(False);
AltPause:=False;

FormMenu.Left:=0;
FormMenu.Top:=0;
FormMenu.Width:=Screen.Width;
FormMenu.Height:=Screen.Height;

ImgRightBorder.Top:=0;
ImgRightBorder.Left:=Screen.Width - ImgRightBorder.Width;

ImgRightBorder1.Top:=Screen.Height - ImgRightBorder1.Height;
ImgRightBorder1.Left:=ImgRightBorder.Left;

ImgLeftBorder1.Top:=Screen.Height - ImgLeftBorder1.Height;
ImgLeftBorder1.Left:=0;

ImgBottomBorder.Top:=Screen.Height - ImgBottomBorder.Height;
ImgBottomBorder.Left:=ImgLeftBorder.Width;

ShockwaveFlash1.Left:=ImgLeftBorder1.Width;
ShockwaveFlash1.Top:=ImgTopBorder.Height;
ShockwaveFlash1.Width:=Screen.Width - ImgLeftBorder1.Width - ImgRightBorder.Width;
ShockwaveFlash1.Height:=Screen.Height - ImgTopBorder.Height - ImgBottomBorder.Height;

ImgScreen.Top:=0;
ImgScreen.Left:=0;
ImgScreen.Width:=Screen.Width;
ImgScreen.Height:=Screen.Height;

ImgBackgr.Top:=0;
ImgBackgr.Left:=0;
ImgBackgr.Width:=Screen.Width;
ImgBackgr.Height:=Screen.Height;
ImgBackgr.Canvas.Brush.Color:=clBlack;
ImgBackgr.Canvas.Rectangle(0,0,Screen.Width,Screen.Height);

Gauge1.Top:=Round(Screen.Height / 1.163636);
Gauge1.Left:=Round(Screen.Width / 13.9130);
Gauge1.Width:=Round(Screen.Width / 1.18);
Gauge1.Height:=Round(Screen.Height / 34.13);

LabelVer.Top:=Round(0.9479167 * Screen.Height);
LabelVer.Left:=Round(0.970703125 * Screen.Width) - LabelVer.Width;

SetVisibleAll(False);
end;

procedure TFormMenu.TimerScreenTimer(Sender: TObject);
begin
Gauge1.Progress:=Gauge1.Progress + Random(20) + 2;
Gauge1.ForeColor:=80 + Gauge1.Progress;
if Gauge1.Progress=100 then
  begin
  SetVisibleAll(True);
  ImgBackgr.Visible:=False;
  Gauge1.Visible:=False;
  LabelVer.Visible:=False;
  ImgScreen.Visible:=False;
  TimerScreen.Enabled:=False;
  end;
end;

procedure TFormMenu.TimerWatchTimer(Sender: TObject);
begin
if ShockwaveFlash1.CurrentFrame >= ShockwaveFlash1.TotalFrames-15 then
  begin
  EndProc;
  end;
Label1.Caption:=FormatTime(ShockwaveFlash1.CurrentFrame)+' / '+FormatTime(ShockwaveFlash1.TotalFrames);
end;

procedure TFormMenu.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if not HidingAll then
  case Key of
  13:                                       //Enter
    begin
    if Playing then
      begin
      if Pause then
        begin
        PauseProc(False);
        end else
        begin
        PauseProc(True);
        end;
      end else
      begin
      StartProc;
      end;
    end;
  32:                                           //Space
    begin
    if Playing then
      begin
      EndProc;
      end else
      begin
      StartProc;
      end;
    end;
  34,39:                                          //Pg Down, Right
    begin
    Image3MouseDown(Image3,MBLeft,[],0,0);
    end;
  33,37:                                          //Pg Up, Left
    begin
    Image2MouseDown(Image3,MBLeft,[],0,0);
    end;
  112:                                          //F1
    begin
    ImgHighInfClick(ImgHighInf);
    end;
  113:                                          //F2
    begin
    ImgHighAbClick(ImgHighAb);
    end;
  115:                                          //F4
    begin
    ImgHighCloseClick(ImgHighClose);
    end;
  end;
end;

procedure TFormMenu.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = 27 then SetVisibleMainMenu(not MainMenuShowing);
end;

procedure TFormMenu.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
CanClose:=False;
ImgHighClose.Visible:=False;
SetVisibleMainMenu(True);
FormQuest.ShowModal;
if FormQuest.Result then
  begin
  CanClose:=True;
  end;
end;

end.
