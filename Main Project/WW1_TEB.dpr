program WW1_TEB;

uses
  Windows, Forms, SysUtils, TGA,
  uMenu in 'uMenu.pas' {FormMenu},
  uQuestion in 'uQuestion.pas' {FormQuest},
  uInform in 'uInform.pas' {FormInf},
  uVideos in 'uVideos.pas' {FormVideos},
  uHrono in 'uHrono.pas' {FormHrono},
  uMemo in 'uMemo.pas' {FormMemo},
  uModalPhotoes in 'uModalPhotoes.pas' {FormModalPh},
  uModalVideos in 'uModalVideos.pas' {FormModalVid},
  uPhotoes in 'uPhotoes.pas' {FormPhotoes},
  uStatistics in 'uStatistics.pas' {FormStat},
  uAbout in 'uAbout.pas' {FormAbout};

var CurrentFile: String;

{$R *.res}

begin
  Application.Initialize;
  Application.Title := '������ �������: ����������� ���� ��������';
  Application.CreateForm(TFormMenu, FormMenu);
  Application.CreateForm(TFormQuest, FormQuest);
  Application.CreateForm(TFormInf, FormInf);
  Application.CreateForm(TFormVideos, FormVideos);
  Application.CreateForm(TFormHrono, FormHrono);
  Application.CreateForm(TFormMemo, FormMemo);
  Application.CreateForm(TFormModalPh, FormModalPh);
  Application.CreateForm(TFormModalVid, FormModalVid);
  Application.CreateForm(TFormPhotoes, FormPhotoes);
  Application.CreateForm(TFormStat, FormStat);
  Application.CreateForm(TFormAbout, FormAbout);
try
  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Textures\Windows\Modal000.tga';
  FormInf.Image1.Picture.LoadFromFile(CurrentFile);
  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Textures\Windows\Modal001.tga';
  FormHrono.Image1.Picture.LoadFromFile(CurrentFile);
  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Textures\Windows\Modal002.tga';
  FormStat.Image1.Picture.LoadFromFile(CurrentFile);
  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Textures\Windows\Modal003.tga';
  FormModalPh.Image1.Picture.LoadFromFile(CurrentFile);
  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Textures\Windows\Modal004.tga';
  FormPhotoes.Image1.Picture.LoadFromFile(CurrentFile);
  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Textures\Windows\Modal005.tga';
  FormModalVid.Image1.Picture.LoadFromFile(CurrentFile);
  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Textures\Windows\Modal006.tga';
  FormVideos.Image1.Picture.LoadFromFile(CurrentFile);
  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Textures\Windows\Modal007.tga';
  FormMemo.Image1.Picture.LoadFromFile(CurrentFile);
  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Textures\Windows\Modal008.tga';
  FormAbout.Image1.Picture.LoadFromFile(CurrentFile);
  CurrentFile:=ExtracTFilePath(ParamStr(0))+'Textures\Windows\Modal009.tga';
  FormQuest.Image1.Picture.LoadFromFile(CurrentFile);
  except
    Application.MessageBox(PChar('������ ��� ������ �����'+#13+CurrentFile
            +#13#13+'���������� ������ ��������� ����������'),
            PChar(FormMenu.Caption),MB_ICONERROR+mb_OK);
    Application.Terminate;
    end;

  Application.Run;
end.
