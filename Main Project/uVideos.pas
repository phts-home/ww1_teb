unit uVideos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, MPlayer, TGA, StdCtrls;

type
  TFormVideos = class(TForm)
    Image1: TImage;
    MediaPlayer1: TMediaPlayer;
    Panel1: TPanel;
    PanelClose: TPanel;
    ImgNormCls: TImage;
    ImgHighLCls: TImage;
    PanelClose2: TPanel;
    ImgNormClose: TImage;
    ImgHighClose: TImage;
    PanelPlay: TPanel;
    ImgNormPlay: TImage;
    ImgHighPlay: TImage;
    PanelStop: TPanel;
    ImgNormSt: TImage;
    ImgHighSt: TImage;
    procedure ImgHighCloseClick(Sender: TObject);
    procedure ImgHighLClsClick(Sender: TObject);
    procedure ImgNormClsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormCloseMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormPlayMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormStMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgHighStClick(Sender: TObject);
    procedure ImgHighPlayClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
  public
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
  end;

var
  FormVideos: TFormVideos;

implementation

{$R *.dfm}

procedure TFormVideos.WMNCHitTest(var M: TWMNCHitTest);
begin
inherited;
if M.Result = htClient then M.Result := htCaption;
ImgHighLCls.Visible:=False;
ImgHighPlay.Visible:=False;
ImgHighSt.Visible:=False;
ImgHighClose.Visible:=False;
end;


//---- ������ ----

//������� [x]
procedure TFormVideos.ImgHighLClsClick(Sender: TObject);
begin
Close;
end;

//�����
procedure TFormVideos.ImgHighPlayClick(Sender: TObject);
begin
MediaPlayer1.Play;
end;

//����
procedure TFormVideos.ImgHighStClick(Sender: TObject);
begin
MediaPlayer1.Stop;
MediaPlayer1.Rewind;
end;

//�������
procedure TFormVideos.ImgHighCloseClick(Sender: TObject);
begin
Close;
end;


//---- ����� ������ ----


//---- ��������� ----

procedure TFormVideos.ImgNormClsMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighLCls.Visible:=True;
end;

procedure TFormVideos.ImgNormPlayMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighPlay.Visible:=True;
end;

procedure TFormVideos.ImgNormStMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighSt.Visible:=True;
end;

procedure TFormVideos.ImgNormCloseMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighClose.Visible:=True;
end;

//---- ����� ��������� ----


procedure TFormVideos.FormClose(Sender: TObject; var Action: TCloseAction);
begin
MediaPlayer1.Stop;
MediaPlayer1.Close;
end;

procedure TFormVideos.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
case Key of
  13,32: ImgHighPlayClick(ImgHighPlay);
  end;
end;

procedure TFormVideos.FormShow(Sender: TObject);
begin
ImgHighLCls.Visible:=False;
ImgHighPlay.Visible:=False;
ImgHighSt.Visible:=False;
ImgHighClose.Visible:=False;

MediaPlayer1.DisplayRect:=Panel1.ClientRect;
end;

procedure TFormVideos.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = 27 then Close;
end;

end.
