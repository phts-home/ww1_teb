unit uModalVideos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, TGA;

type
  TFormModalVid = class(TForm)
    Image1: TImage;
    PanelClose: TPanel;
    ImgNormCls: TImage;
    ImgHighLCls: TImage;
    PanelClose2: TPanel;
    ImgNormClose: TImage;
    ImgHighClose: TImage;
    Panel1: TPanel;
    ImgNorm1: TImage;
    ImgHigh1: TImage;
    procedure ImgHigh1Click(Sender: TObject);
    procedure ImgNorm1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgHighLClsClick(Sender: TObject);
    procedure ImgNormClsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgHighCloseClick(Sender: TObject);
    procedure ImgNormCloseMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
  public
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
  end;

var
  FormModalVid: TFormModalVid;

implementation

uses uVideos;

{$R *.dfm}

procedure TFormModalVid.WMNCHitTest(var M: TWMNCHitTest);
begin
inherited;
if M.Result = htClient then M.Result := htCaption;
ImgHighLCls.Visible:=False;
ImgHigh1.Visible:=False;
ImgHighClose.Visible:=False;
end;


//---- ������ ----

//������� [x]
procedure TFormModalVid.ImgHighLClsClick(Sender: TObject);
begin
Close;
end;

//������ �������
procedure TFormModalVid.ImgHigh1Click(Sender: TObject);
begin
FormVideos.Panel1.Width:=400;
FormVideos.Panel1.Height:=300;
FormVideos.Panel1.Top:=25;
FormVideos.Panel1.Left:=100;
FormVideos.MediaPlayer1.Close;
FormVideos.MediaPlayer1.FileName:=ExtractFilePath(ParamStr(0))+'\Data\Videos\Video001.avi';
FormVideos.MediaPlayer1.Open;
FormVideos.MediaPlayer1.Play;
FormVideos.ShowModal;
end;

//�������
procedure TFormModalVid.ImgHighCloseClick(Sender: TObject);
begin
Close;
end;

//---- ����� ������ ----


//---- ��������� ----

procedure TFormModalVid.ImgNormClsMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
ImgHighLCls.Visible:=True;
end;

procedure TFormModalVid.ImgNorm1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
ImgHigh1.Visible:=True;
end;

procedure TFormModalVid.ImgNormCloseMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
ImgHighClose.Visible:=True;
end;

//---- ����� ��������� ----


procedure TFormModalVid.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
case Key of
  49,97: ImgHigh1Click(ImgHigh1);
  end;
end;

procedure TFormModalVid.FormShow(Sender: TObject);
begin
ImgHighLCls.Visible:=False;
ImgHigh1.Visible:=False;
ImgHighClose.Visible:=False;
end;

procedure TFormModalVid.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = 27 then Close;
end;

end.
