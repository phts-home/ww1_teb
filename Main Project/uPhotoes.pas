unit uPhotoes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, jpeg, TGA;

type
  TFormPhotoes = class(TForm)
    ImageMain: TImage;
    Label1: TLabel;
    Image1: TImage;
    PanelBack: TPanel;
    ImgNormBack: TImage;
    ImgHighBack: TImage;
    PanelForw: TPanel;
    ImgNormForw: TImage;
    ImgHighForw: TImage;
    PanelClose: TPanel;
    ImgNormCls: TImage;
    ImgHighLCls: TImage;
    PanelClose2: TPanel;
    ImgNormClose: TImage;
    ImgHighClose: TImage;
    procedure FormShow(Sender: TObject);
    procedure ImgNormBackMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgNormForwMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgHighLClsClick(Sender: TObject);
    procedure ImgNormClsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImgHighForwMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ImgHighBackMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ImgHighCloseMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ImgNormCloseMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
  public
    Count, MaxCount: Integer;
    SCount, MPath: String;
    procedure NextPic;
    procedure PrevPic;
    procedure WMNCHitTest(var M: TWMNCHitTest); message wm_NCHitTest;
  end;

var
  FormPhotoes: TFormPhotoes;

implementation

{$R *.dfm}

procedure TFormPhotoes.WMNCHitTest(var M: TWMNCHitTest);
begin
inherited;
if M.Result = htClient then M.Result := htCaption;
ImgHighLCls.Visible:=False;
ImgHighBack.Visible:=False;
ImgHighForw.Visible:=False;
ImgHighClose.Visible:=False;
end;

procedure TFormPhotoes.NextPic;
begin
Inc(Count);
if Count>MaxCount then Count:=1;
SCount:=IntToStr(Count);
case Length(SCount) of
  1: SCount:='00'+SCount;
  2: SCount:='0'+SCount;
  end;
ImageMain.Picture.LoadFromFile(MPath+SCount+'.jpg');
Label1.Caption:=IntToStr(Count)+' / '+IntToStr(MaxCount);
end;

procedure TFormPhotoes.PrevPic;
begin
Dec(Count);
if Count<1 then Count:=MaxCount;
SCount:=IntToStr(Count);
case Length(SCount) of
  1: SCount:='00'+SCount;
  2: SCount:='0'+SCount;
  end;
ImageMain.Picture.LoadFromFile(MPath+SCount+'.jpg');
Label1.Caption:=IntToStr(Count)+' / '+IntToStr(MaxCount);
end;


//---- ������ ----

//������� [x]
procedure TFormPhotoes.ImgHighLClsClick(Sender: TObject);
begin
Close;
end;

//������
procedure TFormPhotoes.ImgHighForwMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
NextPic;
end;

//�����
procedure TFormPhotoes.ImgHighBackMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
PrevPic;
end;

//�������
procedure TFormPhotoes.ImgHighCloseMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
Close;
end;

//---- ����� ������ ----


//---- ��������� ----

procedure TFormPhotoes.ImgNormClsMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighLCls.Visible:=True;
end;

procedure TFormPhotoes.ImgNormForwMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighForw.Visible:=True;
end;

procedure TFormPhotoes.ImgNormBackMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighBack.Visible:=True;
end;

procedure TFormPhotoes.ImgNormCloseMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
ImgHighClose.Visible:=True;
end;

//---- ����� ��������� ----


procedure TFormPhotoes.FormShow(Sender: TObject);
begin
Count:=1;
Label1.Caption:='1 / '+IntToStr(MaxCount);
ImageMain.Picture.LoadFromFile(MPath+'001.jpg');
ImgHighLCls.Visible:=False;
ImgHighBack.Visible:=False;
ImgHighForw.Visible:=False;
ImgHighClose.Visible:=False;
end;

procedure TFormPhotoes.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
case Key of
  38, 39: NextPic;
  37, 40: PrevPic;
  end;
end;

procedure TFormPhotoes.FormMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
NextPic;
end;

procedure TFormPhotoes.FormMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
PrevPic;
end;

procedure TFormPhotoes.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = 27 then Close;
end;

end.
